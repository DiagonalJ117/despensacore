﻿using DespensasCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.ViewModels
{
    public class MainDashboardVM
    {
        //public List<Asignado> Asignados { get; set; }
        public List<Encuesta> Encuestas { get; set; }
        public List<Candidato> Asignados { get; set; }
        public List<Candidato> NoAsignados { get; set; }

    }
}

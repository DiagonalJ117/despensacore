﻿using DespensasCore.Models;
using DespensasCore.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.ViewModels
{
    public class EncuestaVM
    {
        public int Id { get; set; }
        public int CandidatoId { get; set; }
        public virtual Candidato Candidato { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Fecha de la Llamada")]
        public DateTime FechaLlamada { get; set; }
        [DisplayName("Estatus de la Llamada")]
        public EstatusLlamada EstatusLlamada { get; set; }
        public List<PreguntaVM> Preguntas { get; set; }
        public double TotalPuntos
        {
            get
            {
                double total = 0;
                foreach (var pregunta in Preguntas)
                {
                    total = total + pregunta.Valor;
                }

                return total;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;
using Microsoft.AspNetCore.Identity;
using DespensasCore.Utilities;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using DespensasCore.Extensions;

namespace DespensasCore.Controllers
{
    public class CandidatosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public CandidatosController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }



        // GET: Candidatos
        public async Task<IActionResult> Index()
        {
            
            var tsociales = await _userManager.GetUsersInRoleAsync("Trabajo Social");
            var tasignados = await _context.Asignados.ToListAsync();
            ViewData["UserIdTS"] = new SelectList(tsociales, "Id", "Email");
            ViewData["UserAsignados"] = _context.Asignados.Include(x => x.User).Include(x=>x.Candidato);

            var tproyectos = await _context.Proyectos.ToListAsync();
            ViewData["ProyectoIdTS"] = new SelectList(tproyectos, "Id", "Nombre");

            //Orgnizaciones
            var Org = await _userManager.GetUsersInRoleAsync("Organizacion");
            ViewData["Orgnizaciones"] = new SelectList(Org, "Id", "Email");

            //Para saber quien esta logeado, desde el controlador
            var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);


            //---------------------------------------- CENTINEL QUICKER -------------------------------------------------------
            var mini_centinel = "a";

            var centinel = await _context.Candidatos.Where(r => r.Nombre == mini_centinel).ToListAsync();
            //------------------------------------------------------------------------------------------------------------------

            if (User.IsInRole("Trabajo Social"))
            {
                
                //Flag which indicates the Current User: Indicates trabajador social role
                ViewData["flag"] = "TSocial";

                return View(centinel);
                

            }
            else if (User.IsInRole("Organizacion"))
            {

                //----------------- Get Current User -----------------
                var CurrentUser_id = CurrentUser.Id;

                //Manda el id de la organziacion para hacer posible la adopcion de candidatos
                var raptor =  _context.AdoptanteUsers.Where(a => a.UserId == CurrentUser.Id).FirstOrDefault();
                ViewData["Org_Id"] = raptor.AdoptanteId;


                //Flag which indicates the Current User: Indicates trabajador social role
                ViewData["flag"] = "Org";

                //----------------------------------------------------


                return View(centinel);

            }

            //Flag which indicates the Current User: Indicates Admin role
            ViewData["flag"] = "Admin";

            return View(centinel);
        }


        //Consultar json para datatables de Candidatos
        [HttpPost]
        public async Task<IActionResult> GetCandidatos([FromBody] DtParameters dtParameters)
        {
 

            var searchBy = dtParameters.Search?.Value;

            /*si el buscador esta vacio, ordenar los resultados por Id ascendiente.*/
            var orderCriteria = "Id";
            var orderAscendingDirection = true;

            /*Si el parametro Order (para el ordenamiento lol) esta vacio, entonces se establecen los valores dentro del if*/
            if(dtParameters.Order != null)
            {
                orderCriteria = dtParameters.Columns[dtParameters.Order[0].Column].Data;
                orderAscendingDirection = dtParameters.Order[0].Dir.ToString().ToLower() == "asc";
            }

            var result = _context.Candidatos.AsQueryable();

            //--------------------------------------------------------------------------------------------------------

            if (User.IsInRole("Trabajo Social"))
            {
                //----------------- Get Current User -----------------
                var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);


                var CurrentUser_id = CurrentUser.Id;
                //----------------------------------------------------

                var asignados = await _context.Asignados.Where(x => x.UserId == CurrentUser_id).Include(x => x.Candidato).ToListAsync();

                var encuestasCandidato = _context.Candidatos.Where(x => asignados.Select(c => c.CandidatoId).Contains(x.Id)).AsQueryable();

                result = encuestasCandidato;

            }
            else if (User.IsInRole("Organizacion"))
            {

                //----------------- Get Current User -----------------

                var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);

                var CurrentUser_id = CurrentUser.Id;

                //var CurrentUserAdoptante = _context.AdoptanteUsers.Where(x => x.UserId == CurrentUser.Id).FirstOrDefault();

                //var Adoptante_id = CurrentUserAdoptante.Id;

                //----------------------------------------------------


                //Busca los candidatos que estan asignados
                var candidatos_asignados = _context.Candidatos.Where(x => _context.Asignados.Select(c => c.CandidatoId).Contains(x.Id)).AsQueryable();

                //Busca las encuestas a traves del id del candidato obtenido en candidatos_asignados
                var encuestasCandidato = await _context.Encuestas.Where(x => candidatos_asignados.Select(c => c.Id).Contains(x.CandidatoId)).ToListAsync();

                //Filtra las encuestas obtenidas por el encuestasCandidato si el estatus de la llamada es finalizada
                var encuestasFinalizadas = encuestasCandidato.Where(x => x.EstatusLlamada == EstatusLlamada.Finalizada).ToList();

                //Obtiene los candidatos en base al filtro realizado en el encuestasFinalizadas dando como resultado a los candidatos asignados con un estatus de llamada finalizada
                var candidatosCLlamada = candidatos_asignados.Where(x => encuestasFinalizadas.Select(c => c.CandidatoId).Contains(x.Id)).ToList();

                //----
                /*
                //Obtiene a los candidatos que esten en AsignadoAdoptante en base al Id de la Organizacion
                var adoptados = await _context.AsignadoAdoptante.Where(x => x.AdoptanteId == Adoptante_id).Include(x => x.Candidato).ToListAsync();

                //Busca a los candidatos obtenidos en adoptados para mostrar solo a los candidatos adoptados por la organizacion
                var candidatosAdoptados = await _context.Candidatos.Where(x => adoptados.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();

                //Hace un filtro para buscar solo a los candidatos que no esten adoptados por esa organizacion
                var candidatosNoAdoptados = candidatosCLlamada.ToList().Except(candidatosAdoptados).ToList();
                */

                result = candidatos_asignados;


            }

            //--------------------------------------------------------------------------------------------------------

            if (!string.IsNullOrEmpty(searchBy))
            {
                result = result.Where(r => r.Nombre != null && r.Nombre.ToUpper().Contains(searchBy.ToUpper()) ||
                r.Calle !=null && r.Calle.ToUpper().Contains(searchBy.ToUpper()) ||
                r.Colonia != null && r.Colonia.ToUpper().Contains(searchBy.ToUpper()) || 
                r.Referencias != null && r.Referencias.ToUpper().Contains(searchBy.ToUpper())||
                r.Situacion != null && r.Situacion.ToUpper().Contains(searchBy.ToUpper()) ||
                r.Descripcion != null && r.Situacion.ToUpper().Contains(searchBy.ToUpper()) ||
                r.Telefono != null && r.Telefono.ToUpper().Contains(searchBy.ToUpper()) ||
                r.TelefonoReportante != null && r.TelefonoReportante.ToUpper().Contains(searchBy.ToUpper()) ||
                r.TipoReportante != null && r.TipoReportante.ToUpper().Contains(searchBy.ToUpper()) ||
                r.NombreReportante != null && r.NombreReportante.ToUpper().Contains(searchBy.ToUpper())

                );
            }

            result = orderAscendingDirection ? result.OrderByDynamic(orderCriteria, DtOrderDir.Asc) : result.OrderByDynamic(orderCriteria, DtOrderDir.Asc);

            /*Obtener count de items filtrados y totales*/
            var filteredResultsCount = await result.CountAsync();
            var totalResultsCount = await _context.Candidatos.CountAsync();

            return Json(new DtResult<Candidato>
            {
                Draw = dtParameters.Draw,
                RecordsTotal = totalResultsCount,
                RecordsFiltered = filteredResultsCount,
                Data = await result.Skip(dtParameters.Start).Take(dtParameters.Length).ToListAsync()
            });

        }



        // GET: Candidatos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidatos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidato == null)
            {
                return NotFound();
            }

            return View(candidato);
        }

        // GET: Candidatos/Create
        public IActionResult Create()
        {
            ViewData["Colonias"] = new SelectList(_context.Colonia, "Nombre", "Nombre");
            
            return View();
        }

        // POST: Candidatos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,Calle,Numero,Referencias,Colonia,Situacion,Telefono,DireccionGMaps,TipoReportante,NombreReportante,TelefonoReportante, Id,CreationDate,LastUpdate,IsDeleted")] Candidato candidato)
        {
            if (ModelState.IsValid)
            {
                candidato.CreationDate = DateTime.UtcNow;
                candidato.LastUpdate = DateTime.UtcNow;
                _context.Add(candidato);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(candidato);
        }

        // GET: Candidatos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidatos.FindAsync(id);
            if (candidato == null)
            {
                return NotFound();
            }
            return View(candidato);
        }

        // POST: Candidatos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Calle,Numero,Referencias,Colonia,Situacion,Telefono,DireccionGMaps,TipoReportante,NombreReportante,TelefonoReportante, Id,CreationDate,LastUpdate,IsDeleted")] Candidato candidato)
        {
            if (id != candidato.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    candidato.LastUpdate = DateTime.UtcNow;
                    _context.Update(candidato);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CandidatoExists(candidato.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(candidato);
        }

        // GET: Candidatos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidatos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidato == null)
            {
                return NotFound();
            }

            return View(candidato);
        }

        // POST: Candidatos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidato = await _context.Candidatos.FindAsync(id);
            _context.Candidatos.Remove(candidato);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CandidatoExists(int id)
        {
            return _context.Candidatos.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Controllers
{
    public class DonantesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DonantesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Donantes
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Donantes.Include(d => d.Periodicidad).Include(d => d.TipoDonante);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Donantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donante = await _context.Donantes
                .Include(d => d.Periodicidad)
                .Include(d => d.TipoDonante)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (donante == null)
            {
                return NotFound();
            }

            return View(donante);
        }

        // GET: Donantes/Create
        public IActionResult Create()
        {
            ViewData["PeriodicidadDonacionId"] = new SelectList(_context.PeriodicidadDonaciones, "Id", "Nombre");
            ViewData["TipoDonanteId"] = new SelectList(_context.TipoDonantes, "Id", "Nombre");
            return View();
        }

        // POST: Donantes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,TipoDonanteId,PeriodicidadDonacionId,Id,CreationDate,LastUpdate,IsDeleted")] Donante donante)
        {
            if (ModelState.IsValid)
            {
                _context.Add(donante);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PeriodicidadDonacionId"] = new SelectList(_context.PeriodicidadDonaciones, "Id", "Nombre", donante.PeriodicidadDonacionId);
            ViewData["TipoDonanteId"] = new SelectList(_context.TipoDonantes, "Id", "Nombre", donante.TipoDonanteId);
            return View(donante);
        }

        // GET: Donantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donante = await _context.Donantes.FindAsync(id);
            if (donante == null)
            {
                return NotFound();
            }
            ViewData["PeriodicidadDonacionId"] = new SelectList(_context.PeriodicidadDonaciones, "Id", "Nombre", donante.PeriodicidadDonacionId);
            ViewData["TipoDonanteId"] = new SelectList(_context.TipoDonantes, "Id", "Nombre", donante.TipoDonanteId);
            return View(donante);
        }

        // POST: Donantes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,TipoDonanteId,PeriodicidadDonacionId,Id,CreationDate,LastUpdate,IsDeleted")] Donante donante)
        {
            if (id != donante.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(donante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonanteExists(donante.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PeriodicidadDonacionId"] = new SelectList(_context.PeriodicidadDonaciones, "Id", "Nombre", donante.PeriodicidadDonacionId);
            ViewData["TipoDonanteId"] = new SelectList(_context.TipoDonantes, "Id", "Nombre", donante.TipoDonanteId);
            return View(donante);
        }

        // GET: Donantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donante = await _context.Donantes
                .Include(d => d.Periodicidad)
                .Include(d => d.TipoDonante)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (donante == null)
            {
                return NotFound();
            }

            return View(donante);
        }

        // POST: Donantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var donante = await _context.Donantes.FindAsync(id);
            _context.Donantes.Remove(donante);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonanteExists(int id)
        {
            return _context.Donantes.Any(e => e.Id == id);
        }
    }
}

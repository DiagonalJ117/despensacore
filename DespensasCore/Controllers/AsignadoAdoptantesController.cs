﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;
using Microsoft.AspNetCore.Identity;

namespace DespensasCore.Controllers
{
    public class AsignadoAdoptantesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public AsignadoAdoptantesController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: AsignadoAdoptantes
        public async Task<IActionResult> Index()
        {
            ViewData["Adoptantes"] = _context.AsignadoAdoptante.Include(x => x.Adoptante).Include(x => x.Candidato);

            if (User.IsInRole("Organizacion")) 
            {
                //----------------- Get Current User -----------------
                var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);

                var CurrentUserAdoptante_id = _context.AdoptanteUsers.Where(x => x.UserId == CurrentUser.Id).FirstAsync().Result.AdoptanteId;
                //----------------------------------------------------

                //Obtiene a los candidatos que esten en AsignadoAdoptante en base al Id de la Organizacion
                var adoptados = await _context.AsignadoAdoptante.Where(x => x.AdoptanteId == CurrentUserAdoptante_id).Include(x => x.Candidato).ToListAsync();

                //Busca a los candidatos obtenidos en adoptados para mostrar solo a los candidatos adoptados por la organizacion
                var centinel = await _context.Candidatos.Where(x => adoptados.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();

                return View(centinel);


            }

            var candidatos_adoptados = await _context.Candidatos.Where(x => _context.AsignadoAdoptante.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();
            

            return View(candidatos_adoptados);
        }

        // GET: Candidatos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidatos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidato == null)
            {
                return NotFound();
            }

            return View(candidato);
        }

        // GET: AsignadoAdoptantes/Create
        public IActionResult Create()
        {
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Id");
            ViewData["AdoptanteId"] = new SelectList(_context.AdoptanteUsers, "Id", "Id");
            return View();
        }

        // POST: AsignadoAdoptantes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidatoId,AdoptanteId,Id,CreationDate,LastUpdate,IsDeleted")] AsignadoAdoptante asignadoAdoptante)
        {
            if (ModelState.IsValid)
            {
                _context.Add(asignadoAdoptante);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Id", asignadoAdoptante.CandidatoId);
            ViewData["AdoptanteId"] = new SelectList(_context.AdoptanteUsers, "Id", "Id", asignadoAdoptante.AdoptanteId);
            return View(asignadoAdoptante);
        }

        // GET: Candidatos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidatos.FindAsync(id);
            if (candidato == null)
            {
                return NotFound();
            }
            return View(candidato);
        }

        // POST: Candidatos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Calle,Numero,Referencias,Colonia,Situacion,Telefono,DireccionGMaps,TipoReportante,Id,CreationDate,LastUpdate,IsDeleted")] Candidato candidato)
        {
            if (id != candidato.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    candidato.LastUpdate = DateTime.UtcNow;
                    _context.Update(candidato);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CandidatoExists(candidato.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(candidato);
        }

        // GET: Candidatos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var candidato = await _context.Candidatos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (candidato == null)
            {
                return NotFound();
            }

            return View(candidato);
        }

        // POST: Candidatos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var candidato = await _context.Candidatos.FindAsync(id);
            _context.Candidatos.Remove(candidato);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CandidatoExists(int id)
        {
            return _context.Candidatos.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Controllers
{
    public class AsignadosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AsignadosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Asignados
        public async Task<IActionResult> Index()
        {
            ViewData["UserAsignados"] = _context.Asignados.Include(x => x.User).Include(x => x.Candidato);
            var applicationDbContext = _context.Asignados.Include(a => a.Candidato);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Asignados/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asignado = await _context.Asignados
                .Include(a => a.Candidato)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (asignado == null)
            {
                return NotFound();
            }

            return View(asignado);
        }

        // GET: Asignados/Create
        public IActionResult Create()
        {
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre");
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Nombre");
            return View();
        }

        // POST: Asignados/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidatoId,UserId,Id,CreationDate,LastUpdate,IsDeleted")] Asignado asignado)
        {
            if (ModelState.IsValid)
            {
                _context.Add(asignado);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", asignado.CandidatoId);
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Nombre", asignado.UserId);
            return View(asignado);
        }

        // GET: Asignados/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asignado = await _context.Asignados.FindAsync(id);
            if (asignado == null)
            {
                return NotFound();
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", asignado.CandidatoId);
            return View(asignado);
        }

        // POST: Asignados/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CandidatoId,UserId,Id,CreationDate,LastUpdate,IsDeleted")] Asignado asignado)
        {
            if (id != asignado.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(asignado);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AsignadoExists(asignado.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", asignado.CandidatoId);
            return View(asignado);
        }

        // GET: Asignados/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asignado = await _context.Asignados
                .Include(a => a.Candidato)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (asignado == null)
            {
                return NotFound();
            }

            return View(asignado);
        }

        // POST: Asignados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var asignado = await _context.Asignados.FindAsync(id);
            _context.Asignados.Remove(asignado);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AsignadoExists(int id)
        {
            return _context.Asignados.Any(e => e.Id == id);
        }
    }
}

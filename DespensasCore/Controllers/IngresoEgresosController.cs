﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.FileProviders;
using System.Diagnostics;

namespace DespensasCore.Controllers
{
    public class IngresoEgresosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public IngresoEgresosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: IngresoEgresos
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.IngresoEgresos.Include(i => i.Proveedor).Include(i => i.Donante).Include(i => i.Proyecto).Include(a => a.BankAccount);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: IngresoEgresos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ingresoEgreso = await _context.IngresoEgresos
                .Include(i => i.Proveedor)
                .Include(i => i.Proyecto)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ingresoEgreso == null)
            {
                return NotFound();
            }

            return View(ingresoEgreso);
        }

        // GET: IngresoEgresos/Create
        public IActionResult CreateIngreso(IFormFile files)
        {
            ViewData["DonanteId"] = new SelectList(_context.Donantes, "Id", "Nombre");
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre");
            ViewData["BankAccountId"] = new SelectList(_context.BankAccounts, "Id", "NumCuenta");
            return View();
        }

        // POST: IngresoEgresos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateIngreso(IFormFile files, [Bind("Nombre,TipoRegistro,DonanteId,MontoAntesIVA,MontoDespuesIVA,FechaCompra,ProyectoId,Comprobante,BankAccountId,Id,CreationDate,LastUpdate,IsDeleted")] IngresoEgreso ingresoEgreso)
        {
            if (ModelState.IsValid)
            {
                //------------------------------------------------------------------------------------
                var currentBankAccount = await _context.BankAccounts.Where(a => a.Id == ingresoEgreso.BankAccountId).FirstOrDefaultAsync();

                //Get iva from Amount
                ingresoEgreso.MontoAntesIVA = ingresoEgreso.MontoDespuesIVA / 1.16;
                ingresoEgreso.MontoAntesIVA = Math.Round(ingresoEgreso.MontoAntesIVA, 2);

                currentBankAccount.Monto += ingresoEgreso.MontoDespuesIVA;
                currentBankAccount.Monto = Math.Round(currentBankAccount.Monto, 2);

                _context.Add(ingresoEgreso);
                _context.Update(currentBankAccount);
                await _context.SaveChangesAsync();

                //------------------------------------------------------------------------------------
                if (files != null)
                {
                    if (files.Length > 0)
                    {
                        //Getting FileName
                        var fileName = Path.GetFileName(files.FileName);

                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());

                        //Getting file Extension
                        var fileExtension = Path.GetExtension(fileName);

                        // concatenating  FileName + FileExtension
                        var newFileName = String.Concat(myUniqueFileName, fileExtension);

                        // Combines two strings into a path.
                        var filepath = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images")).Root + $@"\{newFileName}";

                        using (FileStream fs = System.IO.File.Create(filepath))
                        {
                            files.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            ViewData["DonanteId"] = new SelectList(_context.Donantes, "Id", "Nombre", ingresoEgreso.DonanteId);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", ingresoEgreso.ProyectoId);
            ViewData["BankAccountId"] = new SelectList(_context.BankAccounts, "Id", "NumCuenta", ingresoEgreso.BankAccountId);
            return View(ingresoEgreso);
        }


        // GET: IngresoEgresos/Create
        public IActionResult CreateEgreso(IFormFile files)
        {
            ViewData["ProveedorId"] = new SelectList(_context.Proveedores, "Id", "Nombre");
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre");
            ViewData["BankAccountId"] = new SelectList(_context.BankAccounts, "Id", "NumCuenta");
            return View();
        }

        // POST: IngresoEgresos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEgreso(IFormFile files, [Bind("Nombre,TipoRegistro,ProveedorId,MontoAntesIVA,MontoDespuesIVA,FechaCompra,ProyectoId,Comprobante,BankAccountId,Id,CreationDate,LastUpdate,IsDeleted")] IngresoEgreso ingresoEgreso)
        {
            if (ModelState.IsValid)
            {
                //------------------------------------------------------------------------------------
                var currentBankAccount = await _context.BankAccounts.Where(a => a.Id == ingresoEgreso.BankAccountId).FirstOrDefaultAsync();

                //Get iva from Amount
                ingresoEgreso.MontoAntesIVA = ingresoEgreso.MontoDespuesIVA / 1.16;
                ingresoEgreso.MontoAntesIVA = Math.Round(ingresoEgreso.MontoAntesIVA, 2);

                currentBankAccount.Monto -= ingresoEgreso.MontoDespuesIVA;
                currentBankAccount.Monto = Math.Round(currentBankAccount.Monto, 2);

                _context.Add(ingresoEgreso);
                _context.Update(currentBankAccount);
                await _context.SaveChangesAsync();

                //------------------------------------------------------------------------------------
                if (files != null)
                {
                    if (files.Length > 0)
                    {
                        //Getting FileName
                        var fileName = Path.GetFileName(files.FileName);

                        //Assigning Unique Filename (Guid)
                        var myUniqueFileName = Convert.ToString(Guid.NewGuid());

                        //Getting file Extension
                        var fileExtension = Path.GetExtension(fileName);

                        // concatenating  FileName + FileExtension
                        var newFileName = String.Concat(myUniqueFileName, fileExtension);

                        // Combines two strings into a path.
                        var filepath = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Images")).Root + $@"\{newFileName}";

                        using (FileStream fs = System.IO.File.Create(filepath))
                        {
                            files.CopyTo(fs);
                            fs.Flush();
                        }
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            ViewData["ProveedorId"] = new SelectList(_context.Proveedores, "Id", "Nombre", ingresoEgreso.ProveedorId);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", ingresoEgreso.ProyectoId);
            ViewData["BankAccountId"] = new SelectList(_context.BankAccounts, "Id", "NumCuenta", ingresoEgreso.BankAccountId);
            return View(ingresoEgreso);
        }

        // GET: IngresoEgresos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ingresoEgreso = await _context.IngresoEgresos.FindAsync(id);
            if (ingresoEgreso == null)
            {
                return NotFound();
            }
            ViewData["ProveedorId"] = new SelectList(_context.Proveedores, "Id", "Nombre", ingresoEgreso.ProveedorId);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", ingresoEgreso.ProyectoId);
            ViewData["BankAccountId"] = new SelectList(_context.BankAccounts, "Id", "NumCuenta", ingresoEgreso.BankAccountId);
            return View(ingresoEgreso);
        }

        // POST: IngresoEgresos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,TipoRegistro,ProveedorId,MontoAntesIVA,MontoDespuesIVA,FechaCompra,ProyectoId,Comprobante,BankAccountId,Id,CreationDate,LastUpdate,IsDeleted")] IngresoEgreso ingresoEgreso)
        {
            if (id != ingresoEgreso.Id)
            {
                return NotFound();
            }
            //This gets the old information about this transaction
            //----------------------------
            var oldReference = id;

            var snapshotBankAccountId = _context.IngresoEgresos.Where(a => a.Id == oldReference).Select(x => x.BankAccountId).FirstOrDefault();
            var snapshotBankAccount = _context.BankAccounts.Where(a => a.Id == snapshotBankAccountId).FirstOrDefault();
            var snapshotNoIva = _context.IngresoEgresos.Where(a => a.Id == oldReference).Select(x => x.MontoAntesIVA).FirstOrDefault();
            var snapshotIva = _context.IngresoEgresos.Where(a => a.Id == oldReference).Select(x => x.MontoDespuesIVA).FirstOrDefault();
            var snapshotTypeAmount = _context.IngresoEgresos.Where(a => a.Id == oldReference).Select(x => x.TipoRegistro).FirstOrDefault();
            //----------------------------

            if (ModelState.IsValid)
            {
                try
                {
                    //------------------------------------------------------------------------------------
                    var currentBankAccount = await _context.BankAccounts.Where(a => a.Id == ingresoEgreso.BankAccountId).FirstOrDefaultAsync();

                    //Get iva from Amount
                    ingresoEgreso.MontoAntesIVA = ingresoEgreso.MontoDespuesIVA / 1.16;
                    ingresoEgreso.MontoAntesIVA = Math.Round(ingresoEgreso.MontoAntesIVA, 2);

                    if (snapshotBankAccount == currentBankAccount)
                    {
                        //Get the realAmount
                        var realAmount = snapshotIva - ingresoEgreso.MontoDespuesIVA;
                        //Verify if realAmount is not negative
                        if (realAmount < 0)
                        {
                            realAmount *= -1;
                        }

                        //This allows us to know what type of transaction will be commit
                        bool amountFlag = false;
                        if (snapshotIva < ingresoEgreso.MontoDespuesIVA)
                        {
                            amountFlag = true;
                        }

                        if (snapshotTypeAmount == Utilities.TipoRegistro.Ingreso && ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Ingreso)
                        {
                            if (amountFlag == true)
                            {
                                currentBankAccount.Monto += Math.Round(realAmount, 2);
                            }
                            else
                            {
                                currentBankAccount.Monto -= Math.Round(realAmount, 2);
                            }

                        }
                        else if (snapshotTypeAmount == Utilities.TipoRegistro.Ingreso && ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Egreso)
                        {
                            currentBankAccount.Monto -= ingresoEgreso.MontoDespuesIVA;
                        }
                        else if (snapshotTypeAmount == Utilities.TipoRegistro.Egreso && ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Egreso)
                        {
                            if (amountFlag == true)
                            {
                                currentBankAccount.Monto -= Math.Round(realAmount, 2);
                            }
                            else
                            {
                                currentBankAccount.Monto += Math.Round(realAmount, 2);
                            }
                        }
                        else if (snapshotTypeAmount == Utilities.TipoRegistro.Egreso && ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Ingreso)
                        {
                            currentBankAccount.Monto += ingresoEgreso.MontoDespuesIVA;
                        }

                        currentBankAccount.Monto = Math.Round(currentBankAccount.Monto, 2);

                        _context.Update(ingresoEgreso);
                        _context.Update(currentBankAccount);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        if (ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Ingreso)
                        {
                            snapshotBankAccount.Monto -= ingresoEgreso.MontoDespuesIVA;
                            snapshotBankAccount.Monto = Math.Round(snapshotBankAccount.Monto, 2);
                            _context.Update(snapshotBankAccount);

                            currentBankAccount.Monto += ingresoEgreso.MontoDespuesIVA;
                        }
                        else
                        {
                            snapshotBankAccount.Monto += ingresoEgreso.MontoDespuesIVA;
                            snapshotBankAccount.Monto = Math.Round(snapshotBankAccount.Monto, 2);
                            _context.Update(snapshotBankAccount);

                            currentBankAccount.Monto -= ingresoEgreso.MontoDespuesIVA;
                        }

                        currentBankAccount.Monto = Math.Round(currentBankAccount.Monto, 2);

                        _context.Update(ingresoEgreso);
                        _context.Update(currentBankAccount);
                        await _context.SaveChangesAsync();

                    }
                    //------------------------------------------------------------------------------------
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!IngresoEgresoExists(ingresoEgreso.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProveedorId"] = new SelectList(_context.Proveedores, "Id", "Nombre", ingresoEgreso.ProveedorId);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", ingresoEgreso.ProyectoId);
            ViewData["BankAccountId"] = new SelectList(_context.BankAccounts, "Id", "NumCuenta", ingresoEgreso.BankAccountId);
            return View(ingresoEgreso);
        }

        // GET: IngresoEgresos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ingresoEgreso = await _context.IngresoEgresos
                .Include(i => i.Proveedor)
                .Include(i => i.Proyecto)
                .Include(a => a.BankAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (ingresoEgreso == null)
            {
                return NotFound();
            }

            return View(ingresoEgreso);
        }

        // POST: IngresoEgresos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ingresoEgreso = await _context.IngresoEgresos.FindAsync(id);
            _context.IngresoEgresos.Remove(ingresoEgreso);
            await _context.SaveChangesAsync();

            //------------------------------------------------------------
            //Get Id Account
            var temporalBankAccount_Id = ingresoEgreso.BankAccountId;

            //Search and find Id Account into BankAccounts table
            var bankAccount_reference = _context.BankAccounts.Where(a => a.Id == temporalBankAccount_Id).FirstOrDefault();

            //Verify if that account still in the DataBase
            if (bankAccount_reference != null)
            {
                //If typeAmount is "Ingreso" Account's Mount will decrease. (Reverse Effect)
                if (ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Ingreso)
                {
                    bankAccount_reference.Monto -= ingresoEgreso.MontoDespuesIVA;
                }//If typeAmount is "Egreso" Account's Mount will increase. (Reverse Effect)
                else if (ingresoEgreso.TipoRegistro == Utilities.TipoRegistro.Egreso)
                {
                    bankAccount_reference.Monto += ingresoEgreso.MontoDespuesIVA;
                }

                bankAccount_reference.Monto = Math.Round(bankAccount_reference.Monto, 2);

                _context.Update(bankAccount_reference);
                await _context.SaveChangesAsync();
            }

            //------------------------------------------------------------
            return RedirectToAction(nameof(Index));
        }

        private bool IngresoEgresoExists(int id)
        {
            return _context.IngresoEgresos.Any(e => e.Id == id);
        }
    }
}

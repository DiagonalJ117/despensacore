﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Controllers
{
    public class PeriodicidadDonacionesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PeriodicidadDonacionesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PeriodicidadDonaciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.PeriodicidadDonaciones.ToListAsync());
        }

        // GET: PeriodicidadDonaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var periodicidadDonacion = await _context.PeriodicidadDonaciones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (periodicidadDonacion == null)
            {
                return NotFound();
            }

            return View(periodicidadDonacion);
        }

        // GET: PeriodicidadDonaciones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PeriodicidadDonaciones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,Id,IsDeleted")] PeriodicidadDonacion periodicidadDonacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(periodicidadDonacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(periodicidadDonacion);
        }

        // GET: PeriodicidadDonaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var periodicidadDonacion = await _context.PeriodicidadDonaciones.FindAsync(id);
            if (periodicidadDonacion == null)
            {
                return NotFound();
            }
            return View(periodicidadDonacion);
        }

        // POST: PeriodicidadDonaciones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Id,IsDeleted")] PeriodicidadDonacion periodicidadDonacion)
        {
            if (id != periodicidadDonacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(periodicidadDonacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PeriodicidadDonacionExists(periodicidadDonacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(periodicidadDonacion);
        }

        // GET: PeriodicidadDonaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var periodicidadDonacion = await _context.PeriodicidadDonaciones
                .FirstOrDefaultAsync(m => m.Id == id);
            if (periodicidadDonacion == null)
            {
                return NotFound();
            }

            return View(periodicidadDonacion);
        }

        // POST: PeriodicidadDonaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var periodicidadDonacion = await _context.PeriodicidadDonaciones.FindAsync(id);
            _context.PeriodicidadDonaciones.Remove(periodicidadDonacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PeriodicidadDonacionExists(int id)
        {
            return _context.PeriodicidadDonaciones.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;
using DespensasCore.ViewModels;
using DespensasCore.Data.Migrations;
using Microsoft.AspNetCore.Identity;
using DespensasCore.Extensions;

namespace DespensasCore.Controllers
{
    public class EncuestasController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<User> _userManager;

        public EncuestasController(ApplicationDbContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Encuestas
        public async Task<IActionResult> Index()
        {
            /*
            //---------------------------------------------------------------------------
            if (User.IsInRole("Trabajo Social"))
            {
                //----------------- Get Current User -----------------
                var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);

                var CurrentUser_id = CurrentUser.Id;
                //----------------------------------------------------

                //Get Asignados
                var asignados = await _context.Asignados.Where(x => x.UserId == CurrentUser_id).Include(x => x.Candidato).ToListAsync();

                //Get Candidatos
                var centinel_1 = await _context.Candidatos.Where(x => asignados.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();

                //Get Encuestas
                var centinel_2 = await _context.Encuestas.Where(x => centinel_1.Select(c => c.Id).Contains(x.CandidatoId)).ToListAsync();

                return View(centinel_2);

            }
            //---------------------------------------------------------------------------
            */

            var applicationDbContext = _context.Encuestas.Include(e => e.Candidato);
            return View(await applicationDbContext.ToListAsync());
        }

        //Consultar json para datatables de Encuestas
        [HttpPost]
        public async Task<IActionResult> GetEncuestas([FromBody] DtParameters dtParameters)
        {
            var searchBy = dtParameters.Search?.Value;

            /*si el buscador esta vacio, ordenar los resultados por Id ascendiente.*/
            var orderCriteria = "CreationDate";
            var orderAscendingDirection = true;

            /*Si el parametro Order (para el ordenamiento lol) esta vacio, entonces se establecen los valores dentro del if*/
            if (dtParameters.Order != null)
            {
                orderCriteria = dtParameters.Columns[dtParameters.Order[0].Column].Data;
                orderAscendingDirection = dtParameters.Order[0].Dir.ToString().ToLower() == "asc";
            }

            var result = _context.Encuestas.Include(x=> x.Candidato).AsQueryable();

            if (User.IsInRole("Trabajo Social"))
            {
                //----------------- Get Current User -----------------
                var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);

                var CurrentUser_id = CurrentUser.Id;
                //----------------------------------------------------

                //Get Asignados
                var asignados = await _context.Asignados.Where(x => x.UserId == CurrentUser_id).Include(x => x.Candidato).ToListAsync();

                //Get Candidatos
                var centinel_1 = await _context.Candidatos.Where(x => asignados.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();

                //Get Encuestas
                var centinel_2 = _context.Encuestas.Where(x => centinel_1.Select(c => c.Id).Contains(x.CandidatoId)).AsQueryable();

                result = centinel_2;

            }

            if (!string.IsNullOrEmpty(searchBy))
            {
                result = result.Where(r => r.Candidato.Nombre == (searchBy.ToUpper()));
            }

            result = orderAscendingDirection ? result.OrderByDynamic(orderCriteria, DtOrderDir.Asc) : result.OrderByDynamic(orderCriteria, DtOrderDir.Asc);

            /*Obtener count de items filtrados y totales*/
            var filteredResultsCount = await result.CountAsync();
            var totalResultsCount = await _context.Encuestas.CountAsync();

            return Json(new DtResult<Encuesta>
            {
                Draw = dtParameters.Draw,
                RecordsTotal = totalResultsCount,
                RecordsFiltered = filteredResultsCount,
                Data = await result.Skip(dtParameters.Start).Take(dtParameters.Length).ToListAsync()
            });

        }

        /*Se consulta la encuesta, sin posibilidad de modificarla.*/
        // GET: Encuestas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var encuesta = await _context.Encuestas
                .Include(e => e.Candidato)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (encuesta == null)
            {
                return NotFound();
            }

            encuesta.Preguntas = await _context.Preguntas.Where(x => x.EncuestaId == encuesta.Id).ToListAsync();

            /*Se pasan los datos de la encuesta al ViewModel para que se reflejen en la misma vista*/
            //var evm = new Encuesta
            //{
            //    Candidato = _context.Candidatos.Find(encuesta.CandidatoId),
            //    FechaLlamada = encuesta.FechaLlamada,
            //    EstatusLlamada = encuesta.EstatusLlamada,
            //    Preguntas = await _context.Preguntas.Where(x => x.EncuestaId == encuesta.Id).ToListAsync(),
            //};

            return View(encuesta);
        }

        // GET: Encuestas/Create
        public IActionResult Create(int CandidatoId)
        {
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre");
            var preguntaItems = _context.PreguntaItems;
            var respuestaItems = _context.RespuestaItems;
            var evm = new EncuestaVM
            {
                FechaLlamada = DateTime.UtcNow,
                CandidatoId = CandidatoId,
                Candidato = _context.Candidatos.Find(CandidatoId),
                Preguntas = new List<PreguntaVM>()
            };
            foreach (var pregunta in preguntaItems)
            {
                PreguntaVM p = new PreguntaVM
                {
                    TextoPregunta = pregunta.TextoPregunta,
                    RespuestaItems = respuestaItems.Where(x => x.PreguntaItemId == pregunta.Id).ToList(),
                    Notas = ""
                    
                };
                evm.Preguntas.Add(p);
            };

            
            return View(evm);
        }

        // POST: Encuestas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidatoId,FechaLlamada,EstatusLlamada,TotalPuntos,Id,CreationDate,LastUpdate,IsDeleted")] Encuesta encuesta)
        {
            if (ModelState.IsValid)
            {
                encuesta.CreationDate = DateTime.UtcNow;
                encuesta.LastUpdate = DateTime.UtcNow;
                _context.Add(encuesta);
                
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", encuesta.CandidatoId);
            return View(encuesta);
        }
        
        public async Task<IActionResult> CrearYEncuestar(int id)
        {

            /*Get latest encuesta del candidato y checar si la latest encuesta esta terminada o no.*/
            var latestEncuesta = await _context.Encuestas.Where(x => x.CandidatoId == id && (x.EstatusLlamada != Utilities.EstatusLlamada.Finalizada)).OrderByDescending(x => x.CreationDate).FirstOrDefaultAsync();
            Encuesta nEncuesta = null;
            if (latestEncuesta != null)
            {
                nEncuesta = latestEncuesta;
            }
            else
            {

                nEncuesta = new Encuesta
                {
                
                    CandidatoId = id,
                    /*Se supone que aqui se guardan las timestamps pero por alguna razón no lo hacen. Bug.*/
                    CreationDate = DateTime.UtcNow,
                    LastUpdate = DateTime.UtcNow
                };
                var v = await _context.AddAsync(nEncuesta);
                await _context.SaveChangesAsync();
            }   
            
            return RedirectToAction("Encuestar", "Encuestas", new { id = nEncuesta.Id });

        }


        public async Task<IActionResult> Encuestar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var encuesta = await _context.Encuestas.FindAsync(id);

            
            var preguntaItems = _context.PreguntaItems;
            var respuestaItems = _context.RespuestaItems;
            /*Se crea un nuevo objeto EncuestaVM.
            
            El objeto original de Encuesta guarda solo las preguntas con sus respuestas pero no con sus posibles opciones a elegir en los dropdowns.

            Los ViewModels permiten mostrar datos en una Vista como uno quiera sin modificar el objeto que va a ser guardado en la base de datos.
            En mi caso no quiero que se guarden las opciones de cada pregunta cada vez que se guarde una pregunta respondida en la base de datos pero si quiero ver y poder elegir opciones en la vista para que luego se guarden las respuestas que seleccioné en la base de datos.

            Generalmente utilizo viewmodels cuando necesito traer mas datos de lo que me permite el Modelo original.

             Tal vez necesitemos ver esto mas a fondo jajaj
             */
            var evm = new EncuestaVM
            {
                Id = encuesta.Id,
                FechaLlamada = DateTime.UtcNow,
                CandidatoId = encuesta.CandidatoId,
                Candidato = _context.Candidatos.Find(encuesta.CandidatoId),
                Preguntas = new List<PreguntaVM>()
            };
            /*Recover preguntas from incomplete encuesta*/
            var recoveredQs = _context.Preguntas.Where(x=> x.EncuestaId == encuesta.Id);
            var orderedQs = new List<PreguntaVM>();
            if (recoveredQs.Any())
            {
                foreach(var pregunta in recoveredQs)
                {
                    PreguntaVM p = new PreguntaVM
                    {
                        PreguntaItemId = pregunta.PreguntaItemId,
                        PreguntaItem = await _context.PreguntaItems.Where(x => x.Id == pregunta.PreguntaItemId).FirstAsync(),
                        TextoPregunta = pregunta.TextoPregunta,
                        Respuesta = pregunta.Respuesta,
                        Valor = pregunta.Valor,
                        RespuestaItems = respuestaItems.Where(x => x.PreguntaItem.Id == pregunta.PreguntaItemId).ToList(),
                        Notas = pregunta.Notas

                    };
                    orderedQs.Add(p);
                }

            }
            else
            {

            /*get fresh preguntas*/
                foreach (var pregunta in preguntaItems)
                {
                    PreguntaVM p = new PreguntaVM
                    {
                        PreguntaItemId = pregunta.Id,
                        PreguntaItem = await _context.PreguntaItems.Where(x => x.Id == pregunta.Id).FirstAsync(),
                        TextoPregunta = pregunta.TextoPregunta,
                        RespuestaItems = respuestaItems.Where(x => x.PreguntaItemId == pregunta.Id).ToList(),
                        Notas = ""

                    };
                    orderedQs.Add(p);
                };
            }

            /* Asignar la lista de preguntas ordenadas al ViewModel de Encuesta*/
            evm.Preguntas = orderedQs.OrderBy(x => x.PreguntaItem.NumPregunta).ToList();
            if (encuesta == null)
            {
                return NotFound();
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", encuesta.CandidatoId);
            ViewData["EncuestaId"] = encuesta.Id;
            return View(evm);
        }

        /* Se guarda la encuesta con sus respectivas preguntas en la base de datos */
        [HttpPost]
        public async Task<IActionResult> Encuestar( int? id, [Bind("CandidatoId,FechaLlamada,EstatusLlamada,TotalPuntos,Id")] Encuesta encuesta, List<Pregunta> AnsweredQs)
        {
            if (id != encuesta.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    /*Se itera sobre cada pregunta para crear un objeto pregunta que se ligue a la encuesta*/
                    foreach (var pregunta in AnsweredQs)
                    {
                        Pregunta p = new Pregunta
                        {
                            PreguntaItemId = pregunta.PreguntaItemId,
                            EncuestaId = encuesta.Id, /*El objeto Pregunta se liga a la encuesta por medio de EncuestaId*/
                            TextoPregunta = pregunta.TextoPregunta,
                            Respuesta = pregunta.Respuesta,
                            Valor = pregunta.Valor,
                            Notas = pregunta.Notas
                        };
                        /*Se agrega la pregunta a la cola para guardar en la Base de Datos*/
                        _context.Add(p);
                    };
                   
                    /*Se actualiza la fecha de creacion y de actualización de la encuesta. Es buena practica tener marcas de tiempo (timestamps) para los objetos.*/
                    encuesta.CreationDate = DateTime.UtcNow; /*Se supone que la fecha de creacion se agregaba en el metodo de CrearYEncuestar() pero por algo no lo hacia. Bug.*/
                    encuesta.LastUpdate = DateTime.UtcNow;
                    /*Se actualiza la Encuesta*/
                    _context.Update(encuesta);

                    /*Se guardan todos los cambios en la base de datos*/
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EncuestaExists(encuesta.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", encuesta.CandidatoId);
            return View(encuesta);
        }

        // GET: Encuestas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var encuesta = await _context.Encuestas.FindAsync(id);
            if (encuesta == null)
            {
                return NotFound();
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", encuesta.CandidatoId);
            return View(encuesta);
        }

        // POST: Encuestas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CandidatoId,FechaLlamada,EstatusLlamada,TotalPuntos,Id,CreationDate,LastUpdate,IsDeleted")] Encuesta encuesta, List<Pregunta> AnsweredQs)
        {
            if (id != encuesta.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(encuesta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EncuestaExists(encuesta.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", encuesta.CandidatoId);
            return View(encuesta);
        }

        // GET: Encuestas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var encuesta = await _context.Encuestas
                .Include(e => e.Candidato)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (encuesta == null)
            {
                return NotFound();
            }

            return View(encuesta);
        }

        // POST: Encuestas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var encuesta = await _context.Encuestas.FindAsync(id);
            _context.Encuestas.Remove(encuesta);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EncuestaExists(int id)
        {
            return _context.Encuestas.Any(e => e.Id == id);
        }
    }
}

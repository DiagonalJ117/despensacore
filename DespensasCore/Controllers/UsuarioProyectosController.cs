﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Controllers
{
    public class UsuarioProyectosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UsuarioProyectosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: UsuarioProyectos
        public async Task<IActionResult> Index()
        {
            return View(await _context.UsuarioProyectos.Include(x => x.User).Include(x=>x.Proyecto).ToListAsync());
        }

        // GET: UsuarioProyectos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarioProyecto = await _context.UsuarioProyectos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarioProyecto == null)
            {
                return NotFound();
            }

            return View(usuarioProyecto);
        }

        // GET: UsuarioProyectos/Create
        public IActionResult Create()
        {
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Nombre");
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre");
            return View();
        }

        // POST: UsuarioProyectos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,ProyectoId,Id,CreationDate,LastUpdate,IsDeleted")] UsuarioProyecto usuarioProyecto)
        {
            if (ModelState.IsValid)
            {
                _context.Add(usuarioProyecto);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Nombre", usuarioProyecto.User);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", usuarioProyecto.Proyecto);

            return View(usuarioProyecto);
        }

        // GET: UsuarioProyectos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarioProyecto = await _context.UsuarioProyectos.FindAsync(id);
            if (usuarioProyecto == null)
            {
                return NotFound();
            }
            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Nombre", usuarioProyecto.User);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", usuarioProyecto.Proyecto);
            return View(usuarioProyecto);
        }

        // POST: UsuarioProyectos/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserId,ProyectoId,Id,CreationDate,LastUpdate,IsDeleted")] UsuarioProyecto usuarioProyecto)
        {
            if (id != usuarioProyecto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(usuarioProyecto);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsuarioProyectoExists(usuarioProyecto.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            ViewData["UserId"] = new SelectList(_context.Users, "Id", "Nombre", usuarioProyecto.User);
            ViewData["ProyectoId"] = new SelectList(_context.Proyectos, "Id", "Nombre", usuarioProyecto.Proyecto);

            return View(usuarioProyecto);
        }

        // GET: UsuarioProyectos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarioProyecto = await _context.UsuarioProyectos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarioProyecto == null)
            {
                return NotFound();
            }

            return View(usuarioProyecto);
        }

        // POST: UsuarioProyectos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var usuarioProyecto = await _context.UsuarioProyectos.FindAsync(id);
            _context.UsuarioProyectos.Remove(usuarioProyecto);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UsuarioProyectoExists(int id)
        {
            return _context.UsuarioProyectos.Any(e => e.Id == id);
        }
    }
}

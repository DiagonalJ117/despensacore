﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DespensasCore.Models;
using DespensasCore.Data;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Utilities;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;

namespace DespensasCore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;

        private readonly UserManager<User> _userManager;

        public HomeController(ApplicationDbContext context, ILogger<HomeController> logger, UserManager<User> userManager)
        {
            _context = context;
            _logger = logger;
            _userManager = userManager;
        }

        public async Task<IActionResult> IndexAsync()
        {
            using (_context)
            {
                // codigo Fusionado---------------------------------------------------------------------------
                if (User.IsInRole("Trabajo Social"))
                {
                    //----------------- Get Current User -----------------
                    var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);

                    var CurrentUser_id = CurrentUser.Id;
                    //----------------------------------------------------

                    //Get Asignados
                    var asignados = await _context.Asignados.Where(x => x.UserId == CurrentUser_id).Include(x => x.Candidato).ToListAsync();

                    //Get Candidatos
                    var centinel_1 = await _context.Candidatos.Where(x => asignados.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();

                    //Get Encuestas
                    var centinel_2 = await _context.Encuestas.Where(x => centinel_1.Select(c => c.Id).Contains(x.CandidatoId)).ToListAsync();

                    //Count pendientes
                    var Finalizada = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.Finalizada).Count();
                    var NoContestaron = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NoContestaron).Count();
                    var NoInfo = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NoInfo).Count();
                    var NoConocenPersona = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NoConocenPersona).Count();
                    var NumNoExiste = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NumNoExiste).Count();
                    var CortoLlamada = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.CortoLlamada).Count();

                    //Sum pendientes
                    var todos = NoInfo + NoConocenPersona + NumNoExiste + CortoLlamada + NoContestaron + Finalizada;//_context.Encuestas.Count();
                    var pendientes = NoInfo + NoConocenPersona + NumNoExiste + CortoLlamada;
                    var NoAtendidos = NoContestaron;
                    var encuestados = Finalizada;

                    //Pass variable to View
                    ViewBag.total = todos;
                    ViewBag.pendientes = pendientes;
                    ViewBag.noAtendidos = NoAtendidos;
                    ViewBag.encuestados = encuestados;
                }
                else if (User.IsInRole("Coordinador"))
                {
                    var CurrentUser = await _userManager.GetUserAsync(HttpContext.User);

                    var CurrentUser_id = CurrentUser.Id;
                    //----------------------------------------------------
                    
                    var candidatos_all = await _context.Candidatos.ToListAsync();

                    //Get Asignados
                    var asignados = await _context.Asignados.Include(x => x.Candidato).ToListAsync();

                    //Get Candidatos
                    var centinel_1 = await _context.Candidatos.Where(x => asignados.Select(c => c.CandidatoId).Contains(x.Id)).ToListAsync();

                    //var no_asignados = candidatos_all.ToList().Except(centinel_1).ToList();
                    var no_asignados = candidatos_all.Count() - centinel_1.Count();

                    //Get Encuestas
                    var centinel_2 = await _context.Encuestas.Where(x => centinel_1.Select(c => c.Id).Contains(x.CandidatoId)).ToListAsync();

                    //Count pendientes
                    var TodosAsignados = _context.Asignados.Include(x => x.Candidato).Count();
                    var Finalizada = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.Finalizada).Count();
                    var NoContestaron = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NoContestaron).Count();
                    var NoInfo = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NoInfo).Count();
                    var NoConocenPersona = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NoConocenPersona).Count();
                    var NumNoExiste = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.NumNoExiste).Count();
                    var CortoLlamada = centinel_2.Where(x => x.EstatusLlamada == EstatusLlamada.CortoLlamada).Count();


                    var todos = candidatos_all.Count();//_context.Encuestas.Count();
                    var SiAsignados = TodosAsignados;
                    var NoAsignados = no_asignados;
                    var NoAtendidos = NoContestaron + NoInfo + NoConocenPersona + NumNoExiste + CortoLlamada;
                    var encuestados = Finalizada;

                    ViewBag.total = todos;
                    ViewBag.asignados = SiAsignados;
                    ViewBag.NoAsignados = NoAsignados;
                    ViewBag.noAtendidos = NoAtendidos;
                    ViewBag.encuestados = encuestados;
                }
            }

            //codigo del emma
            //using (_context)
            //{
            //    var Finalizada = _context.Encuestas.Where(x => x.EstatusLlamada == EstatusLlamada.Finalizada).Count();
            //    var NoContestaron = _context.Encuestas.Where(x => x.EstatusLlamada == EstatusLlamada.NoContestaron).Count();
            //    var NoInfo = _context.Encuestas.Where(x => x.EstatusLlamada == EstatusLlamada.NoInfo).Count();
            //    var NoConocenPersona = _context.Encuestas.Where(x => x.EstatusLlamada == EstatusLlamada.NoConocenPersona).Count();
            //    var NumNoExiste = _context.Encuestas.Where(x => x.EstatusLlamada == EstatusLlamada.NumNoExiste).Count();
            //    var CortoLlamada = _context.Encuestas.Where(x => x.EstatusLlamada == EstatusLlamada.CortoLlamada).Count();

            //    var todos = _context.Encuestas.Count();
            //    var pendientes = NoInfo + NoConocenPersona + NumNoExiste + CortoLlamada;
            //    var NoAtendidos = NoContestaron;
            //    var encuestados = Finalizada;

            //    ViewBag.total = todos;
            //    ViewBag.pendientes = pendientes;
            //    ViewBag.noAtendidos = NoAtendidos;
            //    ViewBag.encuestados = encuestados;
            //}

            return View();
        }

        public IActionResult FAQ()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DespensasCore.Data;
using DespensasCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace DespensasCore.Controllers
{
    public class PersonaProyectosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PersonaProyectosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PersonaProyectosController
        public async Task<ActionResult> IndexAsync()
        {
            return View(await _context.PersonaProyectos.Include(x=> x.Candidato).Include(y => y.Proyecto).ToListAsync());
        }

        // GET: PersonaProyectosController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personaproyecto = await _context.PersonaProyectos
                .Include(a => a.Candidato)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (personaproyecto == null)
            {
                return NotFound();
            }

            return View(personaproyecto);
        }

        // GET: PersonaProyectosController/Create
        public IActionResult Create()
        {
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre");
            ViewData["ProyectoId"] = new SelectList(_context.Users, "Id", "Nombre");
            return View();
        }

        // POST: PersonaProyectosController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CandidatoId,ProyectoId,Id,CreationDate,LastUpdate,IsDeleted")] PersonaProyecto personaproyecto)
        {
            if (ModelState.IsValid)
            {
                _context.Add(personaproyecto);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", personaproyecto.CandidatoId);
            ViewData["ProyectoId"] = new SelectList(_context.Users, "Id", "Nombre", personaproyecto.ProyectoId);
            return View(personaproyecto);
        }

        // GET: PersonaProyectosController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personaproyecto = await _context.PersonaProyectos.FindAsync(id);
            if (personaproyecto == null)
            {
                return NotFound();
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", personaproyecto.CandidatoId);
            return View(personaproyecto);
        }

        // POST: PersonaProyectosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CandidatoId,ProyectoId,Id,CreationDate,LastUpdate,IsDeleted")] PersonaProyecto personaproyecto)
        {
            if (id != personaproyecto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(personaproyecto);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonaProyectoExists(personaproyecto.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CandidatoId"] = new SelectList(_context.Candidatos, "Id", "Nombre", personaproyecto.CandidatoId);
            return View(personaproyecto);
        }

        // GET: PersonaProyectosController/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personaproyecto = await _context.PersonaProyectos
                .Include(a => a.Candidato)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (personaproyecto == null)
            {
                return NotFound();
            }

            return View(personaproyecto);
        }

        // POST: PersonaProyectosController/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var personaproyecto = await _context.PersonaProyectos.FindAsync(id);
            _context.PersonaProyectos.Remove(personaproyecto);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PersonaProyectoExists(int id)
        {
            return _context.PersonaProyectos.Any(e => e.Id == id);
        }
    }
}

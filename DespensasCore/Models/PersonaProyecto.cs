﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class PersonaProyecto : Model
    {
        public int CandidatoId { get; set; }
        public virtual Candidato Candidato { get; set; }
        public int ProyectoId { get; set; }
        public virtual Proyecto Proyecto { get; set; }
    }
}

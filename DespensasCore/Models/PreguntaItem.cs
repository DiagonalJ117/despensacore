﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DespensasCore.Models
{
    public class PreguntaItem : Model
    {
        [DisplayName("Numero de Pregunta")]
        public int NumPregunta { get; set; }
        [DisplayName("Texto de Pregunta")]
        public string TextoPregunta { get; set; }
    }
}

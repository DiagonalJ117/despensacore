﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class TipoAdoptante : SimpleModel
    {
        public String Nombre { get; set; }
    }
}

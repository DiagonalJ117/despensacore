﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Pregunta : Model
    {
        public int EncuestaId { get; set; }
        public virtual Encuesta Encuesta { get; set; }
        public int PreguntaItemId { get; set; }
        public virtual PreguntaItem PreguntaItem { get; set; }
        public string TextoPregunta { get; set; }
        public string Respuesta { get; set; }
        public double Valor { get; set; }
        public string Notas { get; set; }
    }
}

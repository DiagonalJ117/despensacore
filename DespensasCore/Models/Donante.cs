﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Donante : Model
    {
        public string Nombre { get; set; }
        [DisplayName("Tipo de Donante")]
        public int TipoDonanteId { get; set; }
        public virtual TipoDonante TipoDonante { get; set; }
        [DisplayName("Periodicidad de Donacion")]
        public int PeriodicidadDonacionId { get; set; }
        public virtual PeriodicidadDonacion Periodicidad { get; set; }
    }
}

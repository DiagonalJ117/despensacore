﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DespensasCore.Models
{
    public class Proyecto : Model
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        [DisplayName("Fecha de Inicio")]
        public DateTime FechaIncio { get; set; }
        [DisplayName("Fecha de Finalización")]
        public DateTime FechaFin { get; set; }
        public double Presupuesto { get; set; }
        [DisplayName("Número de Aceptados")]
        public int NumeroAceptados { get; set; }
        public int? ProgramaId { get; set; }
        public virtual Programa Programa { get; set; }

    }
}

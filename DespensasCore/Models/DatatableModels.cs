﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DespensasCore.Models
{
    /*Archivo de referencia: https://github.com/DavidSuescunPelegay/jQuery-datatable-server-side-net-core/blob/net-core-31/src/jQueryDatatableServerSideNetCore/Models/AuxiliaryModels/DatatableModels.cs */
    /// Resultado completo de una datatable. el formato del resultado que arroja un datatable
    /// T = Tipo de dato de cada fila
    public class DtResult<T>
    {
        /*Agarra la propiedad 'draw' del ajax del front */
        [JsonProperty("draw")]
        public int Draw { get; set; }
        [JsonProperty("recordsTotal")]
        public int RecordsTotal { get; set; }
        [JsonProperty("recordsFiltered")]
        public int RecordsFiltered { get; set; }
        [JsonProperty("data")]
        public IEnumerable<T> Data { get; set; }
        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        public string Error { get; set; }
        public string PartialView { get; set; }

    }

    public abstract class DtRow
    {
        [JsonProperty("DT_RowId")]
        public virtual string DtRowId => null;
        [JsonProperty("DT_RowClass")]
        public virtual string DtRowClass => null;
        [JsonProperty("DT_RowData")]
        public virtual string DtRowData => null;
        [JsonProperty("DT_RowAttr")]
        public virtual object DtRowAttr => null;
    }
    /*Los parametros puestos en el jquery de la datatable*/
    public class DtParameters
    {
        /*contador de draws, o sea que tanto se dibuja y redibuja la datatable en el sitio cuando cambia su contenido.*/
        public int Draw { get; set; }
        /*Array que contiene todas las columnas de la tabla*/
        public DtColumn[] Columns { get; set; }
        public DtOrder[] Order { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public DtSearch Search { get; set; }
        public string SortOrder => Columns != null && Order != null && Order.Length > 0
            ? (Columns[Order[0].Column].Data + (Order[0].Dir == DtOrderDir.Desc ? " " + Order[0].Dir : string.Empty)) : null;
        public IEnumerable<string> AdditionalValues { get; set; }
    }

    public class DtColumn
    {
        /// <summary>
        /// Column's data source, as defined by columns.data.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Column's name, as defined by columns.name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Flag to indicate if this column is searchable (true) or not (false). This is controlled by columns.searchable.
        /// </summary>
        public bool Searchable { get; set; }

        /// <summary>
        /// Flag to indicate if this column is orderable (true) or not (false). This is controlled by columns.orderable.
        /// </summary>
        public bool Orderable { get; set; }

        /// <summary>
        /// Search value to apply to this specific column.
        /// </summary>
        public DtSearch Search { get; set; }
    }

    public class DtOrder
    {
        /// <summary>
        /// Column to which ordering should be applied.
        /// This is an index reference to the columns array of information that is also submitted to the server.
        /// </summary>
        public int Column { get; set; }

        /// <summary>
        /// Ordering direction for this column.
        /// It will be dt-string asc or dt-string desc to indicate ascending ordering or descending ordering, respectively.
        /// </summary>
        public DtOrderDir Dir { get; set; }
    }

    public enum DtOrderDir
    {
        Asc,
        Desc
    }

    public class DtSearch
    {
        /// <summary>
        /// Global search value. To be applied to all columns which have searchable as true.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// true if the global filter should be treated as a regular expression for advanced searching, false otherwise.
        /// Note that normally server-side processing scripts will not perform regular expression searching for performance reasons on large data sets, but it is technically possible and at the discretion of your script.
        /// </summary>
        public bool Regex { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class RegProyecto : Model
    {
        public string Descripcion { get; set; }
        public string Indicadores { get; set; }
        [DisplayName("Linea Base")]
        public string LineaBase { get; set; }
        [DisplayName("Linea de Salida")]
        public string LineaSalida { get; set; }
        [DisplayName("Medios de Verificacion")]
        public string MediosVerificacion { get; set; }
        public string Supuestos { get; set; }
    }
}

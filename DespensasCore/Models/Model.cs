﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Model
    {
        [Display(Name = "Identificador")]
        public int Id { get; set; }
        [Display(Name = "Fecha de Creacion")]
        public DateTime? CreationDate { get; set; } = DateTime.UtcNow;
        [Display(Name = "Ultima Actualizacion")]
        public DateTime? LastUpdate { get; set; } = DateTime.UtcNow;
        [Display(Name = "Borrado")]
        public bool? IsDeleted { get; set; } = false;
    }
}

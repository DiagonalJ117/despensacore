﻿using DespensasCore.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Adoptante : Model
    {
        public String Nombre { get; set; }
        public TipoAdoptante TipoAdoptante { get; set; }
    }
}

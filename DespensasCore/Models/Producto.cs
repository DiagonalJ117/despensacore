﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Producto : RegProyecto
    {
        public int? ResultadoId { get; set; }
        public virtual Resultado Resultado { get; set; }
    }
}

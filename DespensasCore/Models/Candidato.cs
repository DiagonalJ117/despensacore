﻿using DespensasCore.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Candidato : Model
    {
        [DisplayName("Nombre Completo")]
        public string Nombre { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        [DisplayName("Entre calles o referencias")]
        public string Referencias { get; set; }
        public string Colonia { get; set; }
        public String Situacion { get; set; }
        public String Descripcion { get; set; }
        public string Telefono { get; set; }
        [DisplayName("Dirección de Google Maps")]
        public string DireccionGMaps { get; set; }
        [DisplayName("Tipo de Reportante")]
        public String TipoReportante { get; set; }
        [DisplayName("Nombre de Reportante")]
        public String NombreReportante { get; set; }
        [DisplayName("Teléfono de Reportante")]
        public String TelefonoReportante { get; set; }

        /*Con esta propiedad se le asignara la prioridad al candidato segun el puntaje de su mas reciente encuesta 
         */
        public Prioridad Prioridad { get; set; } = Prioridad.NA;
        public int? ProyectoId { get; set; }
        public virtual Proyecto Proyecto { get; set; }

    }
}

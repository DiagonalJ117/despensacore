﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Etapa : Model
    {
        public int? ProyectoId { get; set; }
        public virtual Proyecto Proyecto { get; set; }
        [Required(ErrorMessage = "Por favor Ingrese una fecha")]
        [DisplayName("Año")]
        public DateTime Year { get; set; }
        [Required(ErrorMessage = "Por favor Ingrese una descripcion")]
        [DisplayName("Descripción de la Etapa")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "Por favor Ingrese resultados esperados")]
        [DisplayName("Resultados Esperados")]
        public string Resultados { get; set; }
    }
}

﻿using DespensasCore.Utilities;
using Microsoft.Data.SqlClient.DataClassification;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Encuesta : Model
    {
        public int CandidatoId { get; set; }
        public virtual Candidato Candidato { get; set; }
        [DisplayName("Fecha de Llamada")]
        [DataType(DataType.Date)]
        public DateTime FechaLlamada { get; set; }
        [DisplayName("Estatus de llamada")]
        public EstatusLlamada EstatusLlamada { get; set; } = EstatusLlamada.Incompleta;

        public List<Pregunta> Preguntas { get; set; }
        [DisplayName("Puntos Totales")]
        public double TotalPuntos { get; set; }
        //public double TotalPuntos
        //{
        //    get
        //    {
        //        double total = 0;
        //        foreach(var pregunta in Preguntas)
        //        {
        //            total = total + pregunta.Valor;
        //        }

        //        return total;
        //    }
        //}
    }
}

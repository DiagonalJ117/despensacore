﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DespensasCore.Models
{
    public class RespuestaItem : Model
    {

        public int PreguntaItemId { get; set; }
        [DisplayName("Numero de Pregunta Relacionada")]
        public virtual PreguntaItem PreguntaItem { get; set; }
        [DisplayName("Texto de Respuesta")]
        public string TextoRespuesta { get; set; }
        public int Valor { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class UsuarioProyecto : Model
    {
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public int? ProyectoId { get; set; }
        public virtual Proyecto Proyecto { get; set; }
    }
}

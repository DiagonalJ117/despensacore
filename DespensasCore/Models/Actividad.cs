﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Actividad : RegProyecto
    {
        public int? ProductoId { get; set; }
        public virtual Producto Producto { get; set; }
    }
}

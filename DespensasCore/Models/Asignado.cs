﻿using Microsoft.Data.SqlClient.DataClassification;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Asignado : Model
    {
        public int CandidatoId { get; set; }
        public virtual Candidato Candidato { get; set; }
        [Display(Name = "Identificador del Usuario")]
        public string UserId { get; set; }
        [Display(Name = "Usuario")]
        public virtual User User { get; set; }
    }
}

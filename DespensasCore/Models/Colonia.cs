﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Colonia : Model
    {

        public String CodigoPostal { get; set; }
        public String Nombre { get; set; }
        
        
    }
}

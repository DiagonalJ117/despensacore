﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Tarea: RegProyecto
    {
        public int? ActividadId { get; set; }
        public virtual Actividad Actividad { get; set; }
    }
}

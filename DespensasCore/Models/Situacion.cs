﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Situacion : SimpleModel
    {
        public string Nombre { get; set; }
    }
}

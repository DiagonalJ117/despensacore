﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace DespensasCore.Models
{
    public class SimpleModel
    {
        public int Id { get; set; }
        [DisplayName("Borrado")]
        public bool? IsDeleted { get; set; } = false;
    }
}

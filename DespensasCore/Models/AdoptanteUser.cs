﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class AdoptanteUser : Model
    {
        public int AdoptanteId { get; set; }
        public virtual Adoptante Adoptante { get;set; }
        public String UserId { get; set; }
        public virtual User User { get; set; }
    }
}

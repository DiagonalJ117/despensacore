﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Resultado : RegProyecto
    {
        public int? ProyectoId { get; set; }

        public virtual Proyecto Proyecto { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class AsignadoAdoptante : Model
    {
        public int CandidatoId { get; set; }
        public virtual Candidato Candidato { get; set; }
        public int AdoptanteId { get; set; }
        public virtual Adoptante Adoptante { get; set; }

    }
}

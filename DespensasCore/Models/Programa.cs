﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class Programa : Model
    {
        public string Nombre {get;set;}
        public string Descripcion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class BankAccount : Model
    {
        [DisplayName("Numero de Cuenta")]
        public string NumCuenta { get; set; }
        public string Clabe { get; set; }
        public string Banco { get; set; }
        [DisplayName("Nombre del Propietario")]
        public string NombrePropietario { get; set; }
        public double Monto { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class User : IdentityUser
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }

        public string NombreCompleto
        {
            get
            {
                return Nombre + " " + Apellidos;
            }
        }
    }
}

﻿using DespensasCore.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Models
{
    public class IngresoEgreso : Model
    {
        [DisplayName("Nombre o Descripcion")]
        public string Nombre { get; set; }
        [DisplayName("Ingreso/Egreso")]
        public TipoRegistro TipoRegistro { get; set; } = TipoRegistro.NA;
        [DisplayName("Proveedor")]
        public int? ProveedorId { get; set; }
        public virtual Proveedor Proveedor { get; set; }
        [DisplayName("Donante")]
        public int? DonanteId { get; set; }
        public virtual Donante Donante { get; set; }
        [DisplayName("Monto antes de IVA")]
        public double MontoAntesIVA { get; set; }
        [DisplayName("Monto después de IVA")]
        public double MontoDespuesIVA { get; set; }
        [DisplayName("Fecha de Compra/Donacion")]
        public DateTime FechaCompra { get; set; }
        [DisplayName("Proyecto")]
        public int ProyectoId { get; set; }
        public virtual Proyecto Proyecto { get; set; }
        [DisplayName("Factura o Recibo (Comprobante)")]
        public string Comprobante { get; set; }
        [DisplayName("Cuenta Bancaria Asociada")]
        public int BankAccountId { get; set; }
        [DisplayName("Cuenta Bancaria")]
        public virtual BankAccount BankAccount { get; set; }

        public string NumeroFacturaRecibo { get; set; }
    }
}

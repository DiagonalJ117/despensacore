﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace DespensasCore.Utilities
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum enu)
        {
            var attr = GetDisplayAttribute(enu);
            return attr != null ? attr.Name : enu.ToString();
        }

        public static string GetDescription(this Enum enu)
        {
            var attr = GetDisplayAttribute(enu);
            return attr != null ? attr.Description : enu.ToString();
        }

        private static DisplayAttribute GetDisplayAttribute(object value)
        {
            Type type = value.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException(string.Format("Type {0} is not an enum", type));
            }

            // Get the enum field.
            var field = type.GetField(value.ToString());
            return field?.GetCustomAttribute<DisplayAttribute>();
        }
    }

    public enum EstatusLlamada
    {
        [Display(Name = "No Completada")]
        Incompleta,
        [Display(Name = "Finalizada")]
        Finalizada,
        [Display(Name = "No contestaron")]
        NoContestaron,
        [Display(Name = "No quiso dar informacion")]
        NoInfo,
        [Display(Name = "No conocen a la persona que se buscaba")]
        NoConocenPersona,
        [Display(Name = "El número no existe o esta fuera del área")]
        NumNoExiste,
        [Display(Name = "Se cortó la llamada")]
        CortoLlamada,
    }

    public enum Situacion
    {
        [Display(Name = "Adulto Mayor")]
        AdultoMayor,
        [Display(Name = "Enfermedad Crónica")]
        EnfermedadCronica,
        [Display(Name = "Desempleo")]
        Desempleo,
        [Display(Name = "Otro")]
        Otro
    }

    public enum TipoReportante
    {
        [Display(Name = "Ciudadano")]
        Ciudadano,
        [Display(Name = "Representante de una ONG")]
        RepONG,
        [Display(Name = "Líder Vecinal")]
        LiderVecinal,
    }


    public enum Prioridad
    {
        [Display(Name ="Muy Prioritario")]
        MuyPrioritario,
        [Display(Name = "Prioritario")]
        Prioritario,
        [Display(Name = "No Prioritario")]
        NoPrioritario,
        [Display(Name = "N/A")]
        NA

    }

    public enum TipoAdoptante
    {
        OSC,
        Iniciativa,
        Ciudadano
    }

    public enum TipoRegistro
    {
        [Display(Name = "No Definido")]
        NA,
        Ingreso,
        Egreso
    }

 
}

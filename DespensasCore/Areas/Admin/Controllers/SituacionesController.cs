﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SituacionesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SituacionesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Situaciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.Situacion.ToListAsync());
        }

        // GET: Admin/Situaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var situacion = await _context.Situacion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (situacion == null)
            {
                return NotFound();
            }

            return View(situacion);
        }

        // GET: Admin/Situaciones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Situaciones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,Id,IsDeleted")] Situacion situacion)
        {
            if (ModelState.IsValid)
            {
                _context.Add(situacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(situacion);
        }

        // GET: Admin/Situaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var situacion = await _context.Situacion.FindAsync(id);
            if (situacion == null)
            {
                return NotFound();
            }
            return View(situacion);
        }

        // POST: Admin/Situaciones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Id,IsDeleted")] Situacion situacion)
        {
            if (id != situacion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(situacion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SituacionExists(situacion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(situacion);
        }

        // GET: Admin/Situaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var situacion = await _context.Situacion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (situacion == null)
            {
                return NotFound();
            }

            return View(situacion);
        }

        // POST: Admin/Situaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var situacion = await _context.Situacion.FindAsync(id);
            _context.Situacion.Remove(situacion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SituacionExists(int id)
        {
            return _context.Situacion.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DespensasCore.Areas.Admin.Controllers
{
    public class UserAdminController : Controller
    {
        // GET: UserAdminController
        public ActionResult Index()
        {
            return View();
        }

        // GET: UserAdminController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserAdminController/Create
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult UserRegister()
        {
            return View();
        }

        // POST: UserAdminController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserAdminController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UserAdminController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserAdminController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UserAdminController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}

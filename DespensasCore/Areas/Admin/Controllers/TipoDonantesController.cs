﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TipoDonantesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipoDonantesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/TipoDonantes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoDonantes.ToListAsync());
        }

        // GET: Admin/TipoDonantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoDonante = await _context.TipoDonantes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoDonante == null)
            {
                return NotFound();
            }

            return View(tipoDonante);
        }

        // GET: Admin/TipoDonantes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/TipoDonantes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,Id,IsDeleted")] TipoDonante tipoDonante)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoDonante);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoDonante);
        }

        // GET: Admin/TipoDonantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoDonante = await _context.TipoDonantes.FindAsync(id);
            if (tipoDonante == null)
            {
                return NotFound();
            }
            return View(tipoDonante);
        }

        // POST: Admin/TipoDonantes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Id,IsDeleted")] TipoDonante tipoDonante)
        {
            if (id != tipoDonante.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoDonante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoDonanteExists(tipoDonante.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoDonante);
        }

        // GET: Admin/TipoDonantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoDonante = await _context.TipoDonantes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoDonante == null)
            {
                return NotFound();
            }

            return View(tipoDonante);
        }

        // POST: Admin/TipoDonantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoDonante = await _context.TipoDonantes.FindAsync(id);
            _context.TipoDonantes.Remove(tipoDonante);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoDonanteExists(int id)
        {
            return _context.TipoDonantes.Any(e => e.Id == id);
        }
    }
}

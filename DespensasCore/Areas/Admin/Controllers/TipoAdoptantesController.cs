﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TipoAdoptantesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipoAdoptantesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/TipoAdoptantes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoAdoptante.ToListAsync());
        }

        // GET: Admin/TipoAdoptantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAdoptante = await _context.TipoAdoptante
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoAdoptante == null)
            {
                return NotFound();
            }

            return View(tipoAdoptante);
        }

        // GET: Admin/TipoAdoptantes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/TipoAdoptantes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,Id,IsDeleted")] TipoAdoptante tipoAdoptante)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoAdoptante);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAdoptante);
        }

        // GET: Admin/TipoAdoptantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAdoptante = await _context.TipoAdoptante.FindAsync(id);
            if (tipoAdoptante == null)
            {
                return NotFound();
            }
            return View(tipoAdoptante);
        }

        // POST: Admin/TipoAdoptantes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Id,IsDeleted")] TipoAdoptante tipoAdoptante)
        {
            if (id != tipoAdoptante.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoAdoptante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoAdoptanteExists(tipoAdoptante.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoAdoptante);
        }

        // GET: Admin/TipoAdoptantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoAdoptante = await _context.TipoAdoptante
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoAdoptante == null)
            {
                return NotFound();
            }

            return View(tipoAdoptante);
        }

        // POST: Admin/TipoAdoptantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoAdoptante = await _context.TipoAdoptante.FindAsync(id);
            _context.TipoAdoptante.Remove(tipoAdoptante);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoAdoptanteExists(int id)
        {
            return _context.TipoAdoptante.Any(e => e.Id == id);
        }
    }
}

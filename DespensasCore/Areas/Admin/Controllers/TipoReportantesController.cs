﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TipoReportantesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TipoReportantesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/TipoReportantes
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoReportante.ToListAsync());
        }

        // GET: Admin/TipoReportantes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoReportante = await _context.TipoReportante
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoReportante == null)
            {
                return NotFound();
            }

            return View(tipoReportante);
        }

        // GET: Admin/TipoReportantes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/TipoReportantes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nombre,Id,IsDeleted")] TipoReportante tipoReportante)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tipoReportante);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tipoReportante);
        }

        // GET: Admin/TipoReportantes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoReportante = await _context.TipoReportante.FindAsync(id);
            if (tipoReportante == null)
            {
                return NotFound();
            }
            return View(tipoReportante);
        }

        // POST: Admin/TipoReportantes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Nombre,Id,IsDeleted")] TipoReportante tipoReportante)
        {
            if (id != tipoReportante.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoReportante);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoReportanteExists(tipoReportante.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tipoReportante);
        }

        // GET: Admin/TipoReportantes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoReportante = await _context.TipoReportante
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tipoReportante == null)
            {
                return NotFound();
            }

            return View(tipoReportante);
        }

        // POST: Admin/TipoReportantes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tipoReportante = await _context.TipoReportante.FindAsync(id);
            _context.TipoReportante.Remove(tipoReportante);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TipoReportanteExists(int id)
        {
            return _context.TipoReportante.Any(e => e.Id == id);
        }
    }
}

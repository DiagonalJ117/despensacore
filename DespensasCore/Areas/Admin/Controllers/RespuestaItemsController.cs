﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RespuestaItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RespuestaItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: RespuestaItems
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.RespuestaItems.Include(r => r.PreguntaItem);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: RespuestaItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var respuestaItem = await _context.RespuestaItems
                .Include(r => r.PreguntaItem)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (respuestaItem == null)
            {
                return NotFound();
            }

            return View(respuestaItem);
        }

        // GET: RespuestaItems/Create
        public IActionResult Create()
        {
            ViewData["PreguntaItemId"] = new SelectList(_context.PreguntaItems, "Id", "TextoPregunta");
            return View();
        }

        // POST: RespuestaItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PreguntaItemId,TextoRespuesta,Valor,Id,CreationDate,LastUpdate,IsDeleted")] RespuestaItem respuestaItem)
        {
            if (ModelState.IsValid)
            {
                respuestaItem.CreationDate = DateTime.UtcNow;
                respuestaItem.LastUpdate = DateTime.UtcNow;
                _context.Add(respuestaItem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PreguntaItemId"] = new SelectList(_context.PreguntaItems, "Id", "Id", respuestaItem.PreguntaItemId);
            return View(respuestaItem);
        }

        // GET: RespuestaItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var respuestaItem = await _context.RespuestaItems.FindAsync(id);
            if (respuestaItem == null)
            {
                return NotFound();
            }
            ViewData["PreguntaItemId"] = new SelectList(_context.PreguntaItems, "Id", "TextoPregunta", respuestaItem.PreguntaItemId);
            return View(respuestaItem);
        }

        // POST: RespuestaItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PreguntaItemId,TextoRespuesta,Valor,Id,CreationDate,LastUpdate,IsDeleted")] RespuestaItem respuestaItem)
        {
            if (id != respuestaItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    respuestaItem.LastUpdate = DateTime.UtcNow;
                    _context.Update(respuestaItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RespuestaItemExists(respuestaItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PreguntaItemId"] = new SelectList(_context.PreguntaItems, "Id", "Id", respuestaItem.PreguntaItemId);
            return View(respuestaItem);
        }

        // GET: RespuestaItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var respuestaItem = await _context.RespuestaItems
                .Include(r => r.PreguntaItem)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (respuestaItem == null)
            {
                return NotFound();
            }

            return View(respuestaItem);
        }

        // POST: RespuestaItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var respuestaItem = await _context.RespuestaItems.FindAsync(id);
            _context.RespuestaItems.Remove(respuestaItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RespuestaItemExists(int id)
        {
            return _context.RespuestaItems.Any(e => e.Id == id);
        }
    }
}

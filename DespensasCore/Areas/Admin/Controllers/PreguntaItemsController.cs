﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PreguntaItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PreguntaItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PreguntaItems
        public async Task<IActionResult> Index()
        {
            return View(await _context.PreguntaItems.ToListAsync());
        }

        // GET: PreguntaItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preguntaItem = await _context.PreguntaItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (preguntaItem == null)
            {
                return NotFound();
            }

            return View(preguntaItem);
        }

        // GET: PreguntaItems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PreguntaItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NumPregunta,TextoPregunta,Id,CreationDate,LastUpdate,IsDeleted")] PreguntaItem preguntaItem)
        {
            if (ModelState.IsValid)
            {
                preguntaItem.CreationDate = DateTime.UtcNow;
                preguntaItem.LastUpdate = DateTime.UtcNow;
                _context.Add(preguntaItem);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(preguntaItem);
        }

        // GET: PreguntaItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preguntaItem = await _context.PreguntaItems.FindAsync(id);
            if (preguntaItem == null)
            {
                return NotFound();
            }
            return View(preguntaItem);
        }

        // POST: PreguntaItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NumPregunta,TextoPregunta,Id,CreationDate,LastUpdate,IsDeleted")] PreguntaItem preguntaItem)
        {
            if (id != preguntaItem.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    preguntaItem.LastUpdate = DateTime.UtcNow;
                    _context.Update(preguntaItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PreguntaItemExists(preguntaItem.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(preguntaItem);
        }

        // GET: PreguntaItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preguntaItem = await _context.PreguntaItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (preguntaItem == null)
            {
                return NotFound();
            }

            return View(preguntaItem);
        }

        // POST: PreguntaItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var preguntaItem = await _context.PreguntaItems.FindAsync(id);
            _context.PreguntaItems.Remove(preguntaItem);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PreguntaItemExists(int id)
        {
            return _context.PreguntaItems.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DespensasCore.Data;
using DespensasCore.Models;

namespace DespensasCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ColoniasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ColoniasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Admin/Colonias
        public async Task<IActionResult> Index()
        {
            return View(await _context.Colonia.ToListAsync());
        }

        // GET: Admin/Colonias/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var colonia = await _context.Colonia
                .FirstOrDefaultAsync(m => m.Id == id);
            if (colonia == null)
            {
                return NotFound();
            }

            return View(colonia);
        }

        // GET: Admin/Colonias/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Colonias/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CodigoPostal,Nombre,Id,CreationDate,LastUpdate,IsDeleted")] Colonia colonia)
        {
            if (ModelState.IsValid)
            {
                _context.Add(colonia);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(colonia);
        }

        // GET: Admin/Colonias/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var colonia = await _context.Colonia.FindAsync(id);
            if (colonia == null)
            {
                return NotFound();
            }
            return View(colonia);
        }

        // POST: Admin/Colonias/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CodigoPostal,Nombre,Id,CreationDate,LastUpdate,IsDeleted")] Colonia colonia)
        {
            if (id != colonia.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(colonia);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ColoniaExists(colonia.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(colonia);
        }

        // GET: Admin/Colonias/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var colonia = await _context.Colonia
                .FirstOrDefaultAsync(m => m.Id == id);
            if (colonia == null)
            {
                return NotFound();
            }

            return View(colonia);
        }

        // POST: Admin/Colonias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var colonia = await _context.Colonia.FindAsync(id);
            _context.Colonia.Remove(colonia);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ColoniaExists(int id)
        {
            return _context.Colonia.Any(e => e.Id == id);
        }
    }
}

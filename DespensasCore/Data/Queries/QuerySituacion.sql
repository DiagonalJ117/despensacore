BEGIN TRANSACTION;


SET IDENTITY_INSERT "Situacion" ON ;
INSERT INTO "Situacion" ("Id", "Nombre") VALUES
(1, 'Desempleo'),
(2, 'Adulto Mayor'),
(3, 'Enfermedad Cronica'),
(4, 'Otro')
SET IDENTITY_INSERT "Situacion" OFF;
COMMIT;
BEGIN TRANSACTION
INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Uno', 1, 1),
(GETDATE(), GETDATE(), 0, 'Dos', 1, 1),
(GETDATE(), GETDATE(), 0, 'Tres', 1, 1),
(GETDATE(), GETDATE(), 0, 'Cuatro', 2, 1),
(GETDATE(), GETDATE(), 0, 'Cinco', 2, 1),
(GETDATE(), GETDATE(), 0, 'Seis', 2, 1),
(GETDATE(), GETDATE(), 0, 'Siete', 3, 1),
(GETDATE(), GETDATE(), 0, 'Ocho', 3, 1),
(GETDATE(), GETDATE(), 0, 'Nueve', 3, 1),
(GETDATE(), GETDATE(), 0, 'Diez o más', 4, 1)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Diez o más', 1, 2),
(GETDATE(), GETDATE(), 0, 'Nueve', 1, 2),
(GETDATE(), GETDATE(), 0, 'Ocho', 1, 2),
(GETDATE(), GETDATE(), 0, 'Siete', 2, 2),
(GETDATE(), GETDATE(), 0, 'Seis', 2, 2),
(GETDATE(), GETDATE(), 0, 'Cinco', 2, 2),
(GETDATE(), GETDATE(), 0, 'Cuatro', 3, 2),
(GETDATE(), GETDATE(), 0, 'Tres', 3, 2),
(GETDATE(), GETDATE(), 0, 'Dos', 3, 2),
(GETDATE(), GETDATE(), 0, 'Uno', 4, 2),
(GETDATE(), GETDATE(), 0, 'Cero', 4, 2)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Diez o más', 4, 3),
(GETDATE(), GETDATE(), 0, 'Nueve', 3, 3),
(GETDATE(), GETDATE(), 0, 'Ocho', 3, 3),
(GETDATE(), GETDATE(), 0, 'Siete', 3, 3),
(GETDATE(), GETDATE(), 0, 'Seis', 2, 3),
(GETDATE(), GETDATE(), 0, 'Cinco', 2, 3),
(GETDATE(), GETDATE(), 0, 'Cuatro', 2, 3),
(GETDATE(), GETDATE(), 0, 'Tres', 1, 3),
(GETDATE(), GETDATE(), 0, 'Dos', 1, 3),
(GETDATE(), GETDATE(), 0, 'Uno', 1, 3),
(GETDATE(), GETDATE(), 0, 'Cero', 1, 3)




INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, '3 veces', 1, 4),
(GETDATE(), GETDATE(), 0, '2 veces', 2, 4),
(GETDATE(), GETDATE(), 0, '1 vez', 3, 4),
(GETDATE(), GETDATE(), 0, 'Ninguna', 4, 4)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Si', 1, 5),
(GETDATE(), GETDATE(), 0, 'A veces', 2, 5),
(GETDATE(), GETDATE(), 0, 'No', 3, 5),
(GETDATE(), GETDATE(), 0, 'n/a', 0, 5)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Si', 1, 6),
(GETDATE(), GETDATE(), 0, 'A veces', 2, 6),
(GETDATE(), GETDATE(), 0, 'No', 3, 6),
(GETDATE(), GETDATE(), 0, 'n/a', 0, 6)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Si', 2, 7),
(GETDATE(), GETDATE(), 0, 'No', 1, 7),
(GETDATE(), GETDATE(), 0, 'n/a', 0, 7)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Si', 2, 8),
(GETDATE(), GETDATE(), 0, 'No', 1, 8),
(GETDATE(), GETDATE(), 0, 'n/a', 0, 8)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Cuatro o más', 1, 9),
(GETDATE(), GETDATE(), 0, 'Tres', 2, 9),
(GETDATE(), GETDATE(), 0, 'Dos', 3, 9),
(GETDATE(), GETDATE(), 0, 'Una', 4, 9)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Desempleado', 2, 10),
(GETDATE(), GETDATE(), 0, 'Albañil', 1, 10),
(GETDATE(), GETDATE(), 0, 'Artesano', 1, 10),
(GETDATE(), GETDATE(), 0, 'Carnicero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Carpintero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Cerrajero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Chofer Uber', 1, 10),
(GETDATE(), GETDATE(), 0, 'Costurera', 1, 10),
(GETDATE(), GETDATE(), 0, 'Electricista', 1, 10),
(GETDATE(), GETDATE(), 0, 'Florista', 1, 10),
(GETDATE(), GETDATE(), 0, 'Guardia de seguridad', 1, 10),
(GETDATE(), GETDATE(), 0, 'Jardinero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Maquiladora', 1, 10),
(GETDATE(), GETDATE(), 0, 'Mecánico', 1, 10),
(GETDATE(), GETDATE(), 0, 'Negocio propio', 1, 10),
(GETDATE(), GETDATE(), 0, 'Obrero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Paquetero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Pastelera', 1, 10),
(GETDATE(), GETDATE(), 0, 'Pensionado', 1, 10),
(GETDATE(), GETDATE(), 0, 'Pintor', 1, 10),
(GETDATE(), GETDATE(), 0, 'Plomero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Rotulista', 1, 10),
(GETDATE(), GETDATE(), 0, 'Soldador', 1, 10),
(GETDATE(), GETDATE(), 0, 'Taquero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Tornero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Tortillero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Velador', 1, 10),
(GETDATE(), GETDATE(), 0, 'Vendedor ambulante', 1, 10),
(GETDATE(), GETDATE(), 0, 'Vendedor en puesto fijo', 1, 10),
(GETDATE(), GETDATE(), 0, 'Yesero', 1, 10),
(GETDATE(), GETDATE(), 0, 'Otro', 1, 10)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Comercio informal', 2, 11),
(GETDATE(), GETDATE(), 0, 'Empleo informal', 2, 11),
(GETDATE(), GETDATE(), 0, 'Comercio formal', 1, 11),
(GETDATE(), GETDATE(), 0, 'Empleo formal', 1, 11)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, '$5001 o más', 1, 12),
(GETDATE(), GETDATE(), 0, '$4001 - $5000', 2, 12),
(GETDATE(), GETDATE(), 0, '$3001 - $4000', 3, 12),
(GETDATE(), GETDATE(), 0, '$2001 - $3000', 4, 12),
(GETDATE(), GETDATE(), 0, '$1001 - $2000', 5, 12),
(GETDATE(), GETDATE(), 0, '$1.00 - $1000', 6, 12),
(GETDATE(), GETDATE(), 0, '$0.00', 7, 12),
(GETDATE(), GETDATE(), 0, 'No contestó', 0, 12)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Sí, aumentó', 1, 13),
(GETDATE(), GETDATE(), 0, 'No, es el mismo', 2, 13),
(GETDATE(), GETDATE(), 0, 'Sí, disminuyó', 3, 13),
(GETDATE(), GETDATE(), 0, 'No contestó', 0, 13)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, '$0.00', 1, 12),
(GETDATE(), GETDATE(), 0, '$1.00 - $1000', 1, 14),
(GETDATE(), GETDATE(), 0, '$1001 - $2000', 1, 14),
(GETDATE(), GETDATE(), 0, '$2001 - $3000', 1, 14),
(GETDATE(), GETDATE(), 0, '$3001 - $4000', 1, 14),
(GETDATE(), GETDATE(), 0, '$4001 - $5000', 1, 14),
(GETDATE(), GETDATE(), 0, '$6000 o más', 1, 14),
(GETDATE(), GETDATE(), 0, 'No contestó', 0, 14)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Menor', 1, 15),
(GETDATE(), GETDATE(), 0, 'El mismo', 2, 15),
(GETDATE(), GETDATE(), 0, 'Mayor', 3, 15)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, '100%', 1, 16),
(GETDATE(), GETDATE(), 0, '90% - 70%', 1, 16),
(GETDATE(), GETDATE(), 0, '60% - 40%', 1, 16),
(GETDATE(), GETDATE(), 0, '30% – 10%', 1, 16),
(GETDATE(), GETDATE(), 0, '0%', 1, 16)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Si recibe', 1, 17),
(GETDATE(), GETDATE(), 0, 'Recibía pero por COVID-19 se le retiró', 2, 17),
(GETDATE(), GETDATE(), 0, 'No recibe', 2, 17)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Medicamento', 0, 18),
(GETDATE(), GETDATE(), 0, 'Despensa', 0, 18),
(GETDATE(), GETDATE(), 0, 'Dinero', 0, 18),
(GETDATE(), GETDATE(), 0, 'Otro, ¿Cuál?', 0, 18),
(GETDATE(), GETDATE(), 0, 'No recibe', 0, 18)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Gobierno del Estado (Sedesson, DIF estatal.etc)', 0, 19),
(GETDATE(), GETDATE(), 0, 'Ayuntamiento (DIF municipal, Sedemun,etc)', 0, 19),
(GETDATE(), GETDATE(), 0, 'Organización civil', 0, 19),
(GETDATE(), GETDATE(), 0, 'Organización religiosa', 0, 19),
(GETDATE(), GETDATE(), 0, 'Ciudadano(a)', 0, 19),
(GETDATE(), GETDATE(), 0, 'Otro, ¿Cuál?', 0, 19),
(GETDATE(), GETDATE(), 0, 'No recibe', 0, 19)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Ocasionalmente', 0, 20),
(GETDATE(), GETDATE(), 0, 'Cada mes', 0, 20),
(GETDATE(), GETDATE(), 0, 'Cada quincena', 0, 20),
(GETDATE(), GETDATE(), 0, 'Cada semana', 0, 20),
(GETDATE(), GETDATE(), 0, 'Todos los días', 0, 20),
(GETDATE(), GETDATE(), 0, 'No recibe', 0, 20)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Ocasionalmente', 0, 21),
(GETDATE(), GETDATE(), 0, 'Cada mes', 0, 21),
(GETDATE(), GETDATE(), 0, 'Cada quincena', 0, 21),
(GETDATE(), GETDATE(), 0, 'Cada semana', 0, 21),
(GETDATE(), GETDATE(), 0, 'Todos los días', 0, 21),
(GETDATE(), GETDATE(), 0, 'No recibe', 0, 21)


INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'A partir de COVID-19', 1, 22),
(GETDATE(), GETDATE(), 0, 'Desde antes de COVID-19', 2, 22),
(GETDATE(), GETDATE(), 0, 'No recibe', 0, 22)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'No', 0, 23),
(GETDATE(), GETDATE(), 0, 'Si, Senectud 60-70 años', 1, 23),
(GETDATE(), GETDATE(), 0, 'Si, Vejez 71 a 90 años', 2, 23),
(GETDATE(), GETDATE(), 0, 'Si, grande anciano 90 años o más', 3, 23)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'No', 0, 24),
(GETDATE(), GETDATE(), 0, 'Si', 1, 24)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'No', 0, 25),
(GETDATE(), GETDATE(), 0, 'Si', 1, 25)

INSERT INTO RespuestaItems (CreationDate, LastUpdate, IsDeleted, TextoRespuesta, Valor, PreguntaItemId)
VALUES 
(GETDATE(), GETDATE(), 0, 'Menos de un mes', 1, 26),
(GETDATE(), GETDATE(), 0, 'Un mes', 2, 26),
(GETDATE(), GETDATE(), 0, 'En cuanto pase el COVID-19', 3, 26),
(GETDATE(), GETDATE(), 0, 'No sabe', 4, 26),
(GETDATE(), GETDATE(), 0, 'No contestó', 0, 26)

COMMIT
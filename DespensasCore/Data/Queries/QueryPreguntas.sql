
BEGIN TRANSACTION;
SET IDENTITY_INSERT PreguntaItems ON;
  INSERT INTO PreguntaItems (Id, CreationDate, LastUpdate, IsDeleted, TextoPregunta, NumPregunta)
  VALUES
  (1,GETDATE(), GETDATE(), 0, '¿Cuántas personas viven en su domicilio?', 1),
  (2,GETDATE(), GETDATE(), 0, 'De las personas que viven en su domicilio, ¿cuántas son mayores de 18 años?', 2),
  (3,GETDATE(), GETDATE(), 0, 'De las personas que viven en su domicilio, ¿cuántas son menores de 18 años?', 3),
  (4,GETDATE(), GETDATE(), 0, 'Durante su última semana, ¿cuántas veces al día está consumiendo alimento?', 4),
  (5,GETDATE(), GETDATE(), 0, 'En la última semana, ¿la porción de alimento que consumen los menores de 18 años, corresponde a la que debe consumir para satisfacer su hambre?', 5),
  (6,GETDATE(), GETDATE(), 0, 'En la última semana, ¿la porción de alimento que consumen los mayores de 18 años, corresponde a lo que debe consumir para satisfacer su hambre?', 6),
  (7,GETDATE(), GETDATE(), 0, '¿La situación actual que menciona es diferente a la que tenía antes de COVID-19 para los menores de 18 años', 7),
  (8,GETDATE(), GETDATE(), 0, '¿La situación actual que menciona es diferente a la que tenía antes de COVID-19 para los mayores de 18 años?', 8),
  (9,GETDATE(), GETDATE(), 0, '¿Cuántas personas aportan al ingreso familiar?', 9),
  (10,GETDATE(), GETDATE(), 0, '¿A qué se dedican las personas que aportan al ingreso familiar?', 10),
  (11,GETDATE(), GETDATE(), 0, '¿Cuál es el tipo de empleo de la(s) persona(s) que aportan al ingreso familiar?', 11),
  (12,GETDATE(), GETDATE(), 0, 'En el último mes, ¿cuál fue el ingreso total del hogar? Aquí se pueden apoyar un poco primero preguntando cuánto es su ingreso a la semana, diario según el tipo de empleo.', 12),
  (13,GETDATE(), GETDATE(), 0, 'El ingreso actual que nos menciona ¿ha variado por COVID-19?', 13),
  (14,GETDATE(), GETDATE(), 0, 'En el último mes, ¿cuál fue el total de gastos fijos del hogar? ', 14),
  (15,GETDATE(), GETDATE(), 0, '¿El monto de gastos del mes pasado con respecto de este mes es...?', 15),
  (16,GETDATE(), GETDATE(), 0, '¿Qué cantidad/porcentaje del ingreso total del hogar se destina a la compra de alimento? ', 16),
  (17,GETDATE(), GETDATE(), 0, '¿Actualmente en su hogar se recibe algún tipo de ayuda social?', 17),
  (18,GETDATE(), GETDATE(), 0, '¿Qué tipo de ayuda social recibe en su hogar?', 18),
  (19,GETDATE(), GETDATE(), 0, '¿De quién recibe la ayuda social?', 19),
  (20,GETDATE(), GETDATE(), 0, '¿Con qué frecuencia recibe la ayuda social?', 20),
  (21,GETDATE(), GETDATE(), 0, '¿Cuándo fue la última vez que recibió ese apoyo?', 21),
  (22,GETDATE(), GETDATE(), 0, '¿Hace cuánto tiempo que recibe la ayuda social?', 22),
  (23,GETDATE(), GETDATE(), 0, 'Actualmente, en su hogar ¿Habitan personas de la tercera edad?', 23),
  (24,GETDATE(), GETDATE(), 0, 'Actualmente, en su hogar ¿Habitan personas con capacidades diferentes que requieran en éste momento atención especializada?', 24),
  (25,GETDATE(), GETDATE(), 0, 'Actualmente, en su hogar ¿Habitan personas con alguna enfermedad crónica - degenerativa que requieran en éste momento atención especializada?', 25),
  (26,GETDATE(), GETDATE(), 0, '¿En cuánto tiempo cree usted que su situación va a mejorar?', 26)
  SET IDENTITY_INSERT PreguntaItems OFF;
  COMMIT;
﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class EtapasFixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProyectoId",
                table: "Etapas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Etapas_ProyectoId",
                table: "Etapas",
                column: "ProyectoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Etapas_Proyectos_ProyectoId",
                table: "Etapas",
                column: "ProyectoId",
                principalTable: "Proyectos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Etapas_Proyectos_ProyectoId",
                table: "Etapas");

            migrationBuilder.DropIndex(
                name: "IX_Etapas_ProyectoId",
                table: "Etapas");

            migrationBuilder.DropColumn(
                name: "ProyectoId",
                table: "Etapas");
        }
    }
}

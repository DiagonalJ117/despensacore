﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class ReportanteCandidato : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NombreReportante",
                table: "Candidatos",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TelefonoReportante",
                table: "Candidatos",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NombreReportante",
                table: "Candidatos");

            migrationBuilder.DropColumn(
                name: "TelefonoReportante",
                table: "Candidatos");
        }
    }
}

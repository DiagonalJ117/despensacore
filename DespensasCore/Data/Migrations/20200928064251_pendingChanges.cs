﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class pendingChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonaProyectos_Candidatos_CandidatoId",
                table: "PersonaProyectos");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonaProyectos_Proyectos_ProyectoId",
                table: "PersonaProyectos");

            migrationBuilder.AlterColumn<int>(
                name: "ProyectoId",
                table: "PersonaProyectos",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CandidatoId",
                table: "PersonaProyectos",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonaProyectos_Candidatos_CandidatoId",
                table: "PersonaProyectos",
                column: "CandidatoId",
                principalTable: "Candidatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonaProyectos_Proyectos_ProyectoId",
                table: "PersonaProyectos",
                column: "ProyectoId",
                principalTable: "Proyectos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonaProyectos_Candidatos_CandidatoId",
                table: "PersonaProyectos");

            migrationBuilder.DropForeignKey(
                name: "FK_PersonaProyectos_Proyectos_ProyectoId",
                table: "PersonaProyectos");

            migrationBuilder.AlterColumn<int>(
                name: "ProyectoId",
                table: "PersonaProyectos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CandidatoId",
                table: "PersonaProyectos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PersonaProyectos_Candidatos_CandidatoId",
                table: "PersonaProyectos",
                column: "CandidatoId",
                principalTable: "Candidatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonaProyectos_Proyectos_ProyectoId",
                table: "PersonaProyectos",
                column: "ProyectoId",
                principalTable: "Proyectos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class cuentaBankLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BankAccountId",
                table: "IngresoEgresos",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_IngresoEgresos_BankAccountId",
                table: "IngresoEgresos",
                column: "BankAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_IngresoEgresos_BankAccounts_BankAccountId",
                table: "IngresoEgresos",
                column: "BankAccountId",
                principalTable: "BankAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IngresoEgresos_BankAccounts_BankAccountId",
                table: "IngresoEgresos");

            migrationBuilder.DropIndex(
                name: "IX_IngresoEgresos_BankAccountId",
                table: "IngresoEgresos");

            migrationBuilder.DropColumn(
                name: "BankAccountId",
                table: "IngresoEgresos");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class modthree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "IngresoEgresos");

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "Proyectos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgramaId",
                table: "Proyectos",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Etapas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Year = table.Column<DateTime>(nullable: false),
                    Descripcion = table.Column<string>(nullable: true),
                    Resultados = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Etapas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Programas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Resultados",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Indicadores = table.Column<string>(nullable: true),
                    LineaBase = table.Column<string>(nullable: true),
                    LineaSalida = table.Column<string>(nullable: true),
                    MediosVerificacion = table.Column<string>(nullable: true),
                    Supuestos = table.Column<string>(nullable: true),
                    ProyectoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resultados", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resultados_Proyectos_ProyectoId",
                        column: x => x.ProyectoId,
                        principalTable: "Proyectos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Productos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Indicadores = table.Column<string>(nullable: true),
                    LineaBase = table.Column<string>(nullable: true),
                    LineaSalida = table.Column<string>(nullable: true),
                    MediosVerificacion = table.Column<string>(nullable: true),
                    Supuestos = table.Column<string>(nullable: true),
                    ResultadoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Productos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Productos_Resultados_ResultadoId",
                        column: x => x.ResultadoId,
                        principalTable: "Resultados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Actividades",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Indicadores = table.Column<string>(nullable: true),
                    LineaBase = table.Column<string>(nullable: true),
                    LineaSalida = table.Column<string>(nullable: true),
                    MediosVerificacion = table.Column<string>(nullable: true),
                    Supuestos = table.Column<string>(nullable: true),
                    ProductoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actividades", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Actividades_Productos_ProductoId",
                        column: x => x.ProductoId,
                        principalTable: "Productos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tareas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Descripcion = table.Column<string>(nullable: true),
                    Indicadores = table.Column<string>(nullable: true),
                    LineaBase = table.Column<string>(nullable: true),
                    LineaSalida = table.Column<string>(nullable: true),
                    MediosVerificacion = table.Column<string>(nullable: true),
                    Supuestos = table.Column<string>(nullable: true),
                    ActividadId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tareas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tareas_Actividades_ActividadId",
                        column: x => x.ActividadId,
                        principalTable: "Actividades",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Proyectos_ProgramaId",
                table: "Proyectos",
                column: "ProgramaId");

            migrationBuilder.CreateIndex(
                name: "IX_Actividades_ProductoId",
                table: "Actividades",
                column: "ProductoId");

            migrationBuilder.CreateIndex(
                name: "IX_Productos_ResultadoId",
                table: "Productos",
                column: "ResultadoId");

            migrationBuilder.CreateIndex(
                name: "IX_Resultados_ProyectoId",
                table: "Resultados",
                column: "ProyectoId");

            migrationBuilder.CreateIndex(
                name: "IX_Tareas_ActividadId",
                table: "Tareas",
                column: "ActividadId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proyectos_Programas_ProgramaId",
                table: "Proyectos",
                column: "ProgramaId",
                principalTable: "Programas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proyectos_Programas_ProgramaId",
                table: "Proyectos");

            migrationBuilder.DropTable(
                name: "Etapas");

            migrationBuilder.DropTable(
                name: "Programas");

            migrationBuilder.DropTable(
                name: "Tareas");

            migrationBuilder.DropTable(
                name: "Actividades");

            migrationBuilder.DropTable(
                name: "Productos");

            migrationBuilder.DropTable(
                name: "Resultados");

            migrationBuilder.DropIndex(
                name: "IX_Proyectos_ProgramaId",
                table: "Proyectos");

            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "Proyectos");

            migrationBuilder.DropColumn(
                name: "ProgramaId",
                table: "Proyectos");

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "IngresoEgresos",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}

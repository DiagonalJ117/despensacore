﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class PersonasProyectos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProyectoId",
                table: "Candidatos",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Proyectos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    FechaIncio = table.Column<DateTime>(nullable: false),
                    FechaFin = table.Column<DateTime>(nullable: false),
                    Presupuesto = table.Column<double>(nullable: false),
                    NumeroAceptados = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proyectos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersonaProyectos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    PersonaId = table.Column<string>(nullable: true),
                    ProyectoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonaProyectos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PersonaProyectos_AspNetUsers_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PersonaProyectos_Proyectos_ProyectoId",
                        column: x => x.ProyectoId,
                        principalTable: "Proyectos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Candidatos_ProyectoId",
                table: "Candidatos",
                column: "ProyectoId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonaProyectos_PersonaId",
                table: "PersonaProyectos",
                column: "PersonaId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonaProyectos_ProyectoId",
                table: "PersonaProyectos",
                column: "ProyectoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Candidatos_Proyectos_ProyectoId",
                table: "Candidatos",
                column: "ProyectoId",
                principalTable: "Proyectos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Candidatos_Proyectos_ProyectoId",
                table: "Candidatos");

            migrationBuilder.DropTable(
                name: "PersonaProyectos");

            migrationBuilder.DropTable(
                name: "Proyectos");

            migrationBuilder.DropIndex(
                name: "IX_Candidatos_ProyectoId",
                table: "Candidatos");

            migrationBuilder.DropColumn(
                name: "ProyectoId",
                table: "Candidatos");
        }
    }
}

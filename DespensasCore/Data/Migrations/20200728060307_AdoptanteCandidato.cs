﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class AdoptanteCandidato : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AsignadoAdoptante_AspNetUsers_UserId",
                table: "AsignadoAdoptante");

            migrationBuilder.DropIndex(
                name: "IX_AsignadoAdoptante_UserId",
                table: "AsignadoAdoptante");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "AsignadoAdoptante");

            migrationBuilder.AddColumn<int>(
                name: "AdoptanteId",
                table: "AsignadoAdoptante",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Adoptantes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Nombre = table.Column<string>(nullable: true),
                    TipoAdoptante = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adoptantes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AdoptanteUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    LastUpdate = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    AdoptanteId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdoptanteUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AdoptanteUsers_Adoptantes_AdoptanteId",
                        column: x => x.AdoptanteId,
                        principalTable: "Adoptantes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdoptanteUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AsignadoAdoptante_AdoptanteId",
                table: "AsignadoAdoptante",
                column: "AdoptanteId");

            migrationBuilder.CreateIndex(
                name: "IX_AdoptanteUsers_AdoptanteId",
                table: "AdoptanteUsers",
                column: "AdoptanteId");

            migrationBuilder.CreateIndex(
                name: "IX_AdoptanteUsers_UserId",
                table: "AdoptanteUsers",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AsignadoAdoptante_Adoptantes_AdoptanteId",
                table: "AsignadoAdoptante",
                column: "AdoptanteId",
                principalTable: "Adoptantes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AsignadoAdoptante_Adoptantes_AdoptanteId",
                table: "AsignadoAdoptante");

            migrationBuilder.DropTable(
                name: "AdoptanteUsers");

            migrationBuilder.DropTable(
                name: "Adoptantes");

            migrationBuilder.DropIndex(
                name: "IX_AsignadoAdoptante_AdoptanteId",
                table: "AsignadoAdoptante");

            migrationBuilder.DropColumn(
                name: "AdoptanteId",
                table: "AsignadoAdoptante");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "AsignadoAdoptante",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AsignadoAdoptante_UserId",
                table: "AsignadoAdoptante",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AsignadoAdoptante_AspNetUsers_UserId",
                table: "AsignadoAdoptante",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class preguntaItemIdonPregunta : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PreguntaItemId",
                table: "Preguntas",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Preguntas_PreguntaItemId",
                table: "Preguntas",
                column: "PreguntaItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_Preguntas_PreguntaItems_PreguntaItemId",
                table: "Preguntas",
                column: "PreguntaItemId",
                principalTable: "PreguntaItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Preguntas_PreguntaItems_PreguntaItemId",
                table: "Preguntas");

            migrationBuilder.DropIndex(
                name: "IX_Preguntas_PreguntaItemId",
                table: "Preguntas");

            migrationBuilder.DropColumn(
                name: "PreguntaItemId",
                table: "Preguntas");
        }
    }
}

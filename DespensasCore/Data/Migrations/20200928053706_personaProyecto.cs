﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class personaProyecto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonaProyectos_AspNetUsers_PersonaId",
                table: "PersonaProyectos");

            migrationBuilder.DropIndex(
                name: "IX_PersonaProyectos_PersonaId",
                table: "PersonaProyectos");

            migrationBuilder.DropColumn(
                name: "PersonaId",
                table: "PersonaProyectos");

            migrationBuilder.AddColumn<int>(
                name: "CandidatoId",
                table: "PersonaProyectos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PersonaProyectos_CandidatoId",
                table: "PersonaProyectos",
                column: "CandidatoId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonaProyectos_Candidatos_CandidatoId",
                table: "PersonaProyectos",
                column: "CandidatoId",
                principalTable: "Candidatos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonaProyectos_Candidatos_CandidatoId",
                table: "PersonaProyectos");

            migrationBuilder.DropIndex(
                name: "IX_PersonaProyectos_CandidatoId",
                table: "PersonaProyectos");

            migrationBuilder.DropColumn(
                name: "CandidatoId",
                table: "PersonaProyectos");

            migrationBuilder.AddColumn<string>(
                name: "PersonaId",
                table: "PersonaProyectos",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PersonaProyectos_PersonaId",
                table: "PersonaProyectos",
                column: "PersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonaProyectos_AspNetUsers_PersonaId",
                table: "PersonaProyectos",
                column: "PersonaId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

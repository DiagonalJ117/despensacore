﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class FixedPreguntasResp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RespuestaItems_PreguntaItems_PreguntaItemId",
                table: "RespuestaItems");

            migrationBuilder.AlterColumn<int>(
                name: "PreguntaItemId",
                table: "RespuestaItems",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_RespuestaItems_PreguntaItems_PreguntaItemId",
                table: "RespuestaItems",
                column: "PreguntaItemId",
                principalTable: "PreguntaItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RespuestaItems_PreguntaItems_PreguntaItemId",
                table: "RespuestaItems");

            migrationBuilder.AlterColumn<int>(
                name: "PreguntaItemId",
                table: "RespuestaItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_RespuestaItems_PreguntaItems_PreguntaItemId",
                table: "RespuestaItems",
                column: "PreguntaItemId",
                principalTable: "PreguntaItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

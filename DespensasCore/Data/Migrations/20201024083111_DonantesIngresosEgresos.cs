﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class DonantesIngresosEgresos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PeriodicidadDonaciones",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PeriodicidadDonaciones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Proveedores",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Proveedores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoDonantes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDonantes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IngresoEgresos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    TipoRegistro = table.Column<int>(nullable: false),
                    ProveedorId = table.Column<int>(nullable: false),
                    MontoAntesIVA = table.Column<double>(nullable: false),
                    MontoDespuesIVA = table.Column<double>(nullable: false),
                    FechaCompra = table.Column<DateTime>(nullable: false),
                    ProyectoId = table.Column<int>(nullable: false),
                    Comprobante = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngresoEgresos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IngresoEgresos_Proveedores_ProveedorId",
                        column: x => x.ProveedorId,
                        principalTable: "Proveedores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IngresoEgresos_Proyectos_ProyectoId",
                        column: x => x.ProyectoId,
                        principalTable: "Proyectos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Donantes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationDate = table.Column<DateTime>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true),
                    TipoDonanteId = table.Column<int>(nullable: false),
                    PeriodicidadDonacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Donantes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Donantes_PeriodicidadDonaciones_PeriodicidadDonacionId",
                        column: x => x.PeriodicidadDonacionId,
                        principalTable: "PeriodicidadDonaciones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Donantes_TipoDonantes_TipoDonanteId",
                        column: x => x.TipoDonanteId,
                        principalTable: "TipoDonantes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Donantes_PeriodicidadDonacionId",
                table: "Donantes",
                column: "PeriodicidadDonacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Donantes_TipoDonanteId",
                table: "Donantes",
                column: "TipoDonanteId");

            migrationBuilder.CreateIndex(
                name: "IX_IngresoEgresos_ProveedorId",
                table: "IngresoEgresos",
                column: "ProveedorId");

            migrationBuilder.CreateIndex(
                name: "IX_IngresoEgresos_ProyectoId",
                table: "IngresoEgresos",
                column: "ProyectoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Donantes");

            migrationBuilder.DropTable(
                name: "IngresoEgresos");

            migrationBuilder.DropTable(
                name: "PeriodicidadDonaciones");

            migrationBuilder.DropTable(
                name: "TipoDonantes");

            migrationBuilder.DropTable(
                name: "Proveedores");
        }
    }
}

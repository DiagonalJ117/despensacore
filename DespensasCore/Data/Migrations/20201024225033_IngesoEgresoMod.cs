﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class IngesoEgresoMod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IngresoEgresos_Proveedores_ProveedorId",
                table: "IngresoEgresos");

            migrationBuilder.AlterColumn<int>(
                name: "ProveedorId",
                table: "IngresoEgresos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "IngresoEgresos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DonanteId",
                table: "IngresoEgresos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_IngresoEgresos_DonanteId",
                table: "IngresoEgresos",
                column: "DonanteId");

            migrationBuilder.AddForeignKey(
                name: "FK_IngresoEgresos_Donantes_DonanteId",
                table: "IngresoEgresos",
                column: "DonanteId",
                principalTable: "Donantes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_IngresoEgresos_Proveedores_ProveedorId",
                table: "IngresoEgresos",
                column: "ProveedorId",
                principalTable: "Proveedores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IngresoEgresos_Donantes_DonanteId",
                table: "IngresoEgresos");

            migrationBuilder.DropForeignKey(
                name: "FK_IngresoEgresos_Proveedores_ProveedorId",
                table: "IngresoEgresos");

            migrationBuilder.DropIndex(
                name: "IX_IngresoEgresos_DonanteId",
                table: "IngresoEgresos");

            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "IngresoEgresos");

            migrationBuilder.DropColumn(
                name: "DonanteId",
                table: "IngresoEgresos");

            migrationBuilder.AlterColumn<int>(
                name: "ProveedorId",
                table: "IngresoEgresos",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_IngresoEgresos_Proveedores_ProveedorId",
                table: "IngresoEgresos",
                column: "ProveedorId",
                principalTable: "Proveedores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

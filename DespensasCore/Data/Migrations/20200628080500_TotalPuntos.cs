﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class TotalPuntos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "TotalPuntos",
                table: "Encuestas",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalPuntos",
                table: "Encuestas");
        }
    }
}

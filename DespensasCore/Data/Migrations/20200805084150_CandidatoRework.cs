﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class CandidatoRework : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TipoAdoptante",
                table: "Adoptantes");

            migrationBuilder.AlterColumn<string>(
                name: "TipoReportante",
                table: "Candidatos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "Situacion",
                table: "Candidatos",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Descripcion",
                table: "Candidatos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TipoAdoptanteId",
                table: "Adoptantes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Situacion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Situacion", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoAdoptante",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoAdoptante", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TipoReportante",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsDeleted = table.Column<bool>(nullable: true),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoReportante", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Adoptantes_TipoAdoptanteId",
                table: "Adoptantes",
                column: "TipoAdoptanteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Adoptantes_TipoAdoptante_TipoAdoptanteId",
                table: "Adoptantes",
                column: "TipoAdoptanteId",
                principalTable: "TipoAdoptante",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Adoptantes_TipoAdoptante_TipoAdoptanteId",
                table: "Adoptantes");

            migrationBuilder.DropTable(
                name: "Situacion");

            migrationBuilder.DropTable(
                name: "TipoAdoptante");

            migrationBuilder.DropTable(
                name: "TipoReportante");

            migrationBuilder.DropIndex(
                name: "IX_Adoptantes_TipoAdoptanteId",
                table: "Adoptantes");

            migrationBuilder.DropColumn(
                name: "Descripcion",
                table: "Candidatos");

            migrationBuilder.DropColumn(
                name: "TipoAdoptanteId",
                table: "Adoptantes");

            migrationBuilder.AlterColumn<int>(
                name: "TipoReportante",
                table: "Candidatos",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Situacion",
                table: "Candidatos",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TipoAdoptante",
                table: "Adoptantes",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

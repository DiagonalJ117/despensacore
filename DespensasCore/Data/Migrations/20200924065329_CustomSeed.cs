﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace DespensasCore.Data.Migrations
{
    public partial class CustomSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlPreguntas = Path.Combine(@"./Data/Queries/QueryPreguntas.sql");
            var sqlRespuestaItems = Path.Combine(@"./Data/Queries/QueryRespuestaItems.sql");
            var sqlSituacion = Path.Combine(@"./Data/Queries/QuerySituacion.sql");
            var sqlColonias = Path.Combine(@"./Data/Queries/QueryColonias.sql");
            //Console.WriteLine(File.ReadAllText(sqlFile));
            migrationBuilder.Sql(File.ReadAllText(sqlPreguntas));
            migrationBuilder.Sql(File.ReadAllText(sqlRespuestaItems));
            migrationBuilder.Sql(File.ReadAllText(sqlSituacion));
            migrationBuilder.Sql(File.ReadAllText(sqlColonias));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DespensasCore.Data.Migrations
{
    public partial class correction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Asignados_AspNetUsers_UserId1",
                table: "Asignados");

            migrationBuilder.DropForeignKey(
                name: "FK_Preguntas_Encuestas_EncuestaId",
                table: "Preguntas");

            migrationBuilder.DropIndex(
                name: "IX_Asignados_UserId1",
                table: "Asignados");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Asignados");

            migrationBuilder.AlterColumn<int>(
                name: "EncuestaId",
                table: "Preguntas",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Asignados",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Asignados_UserId",
                table: "Asignados",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Asignados_AspNetUsers_UserId",
                table: "Asignados",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Preguntas_Encuestas_EncuestaId",
                table: "Preguntas",
                column: "EncuestaId",
                principalTable: "Encuestas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Asignados_AspNetUsers_UserId",
                table: "Asignados");

            migrationBuilder.DropForeignKey(
                name: "FK_Preguntas_Encuestas_EncuestaId",
                table: "Preguntas");

            migrationBuilder.DropIndex(
                name: "IX_Asignados_UserId",
                table: "Asignados");

            migrationBuilder.AlterColumn<int>(
                name: "EncuestaId",
                table: "Preguntas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Asignados",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId1",
                table: "Asignados",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Asignados_UserId1",
                table: "Asignados",
                column: "UserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Asignados_AspNetUsers_UserId1",
                table: "Asignados",
                column: "UserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Preguntas_Encuestas_EncuestaId",
                table: "Preguntas",
                column: "EncuestaId",
                principalTable: "Encuestas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

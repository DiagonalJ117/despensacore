﻿using DespensasCore.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DespensasCore.Data
{
    public class DBInit
    {
        public async Task Seed(RoleManager<IdentityRole> roleManager, UserManager<User> userManager, ApplicationDbContext db)
        {
            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Admin";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("Director").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Director";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }

            if (!roleManager.RoleExistsAsync("Coordinador").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Coordinador";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("Trabajo Social").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Trabajo Social";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
            if (!roleManager.RoleExistsAsync("Organizacion").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Organizacion";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }


            //Creating default superuser.
            if (userManager.FindByNameAsync("admin@example.com").Result == null)
            {
                var user = new User
                {
                    Email = "admin@example.com",
                    UserName = "admin@example.com",
                    EmailConfirmed = true,

                };

                IdentityResult result = await userManager.CreateAsync(user, "Test123!");
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, "Admin").Wait();
            }
            if (userManager.FindByNameAsync("trabajosocial@example.com").Result == null)
            {
                var user = new User
                {
                    Email = "trabajosocial@example.com",
                    UserName = "trabajosocial@example.com",
                    EmailConfirmed = true,

                };

                IdentityResult result = await userManager.CreateAsync(user, "Test123!");
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, "Trabajo Social").Wait();
            }
            if (userManager.FindByNameAsync("org@example.com").Result == null)
            {
                var user = new User
                {
                    Email = "org@example.com",
                    UserName = "org@example.com",
                    EmailConfirmed = true,

                };

                IdentityResult result = await userManager.CreateAsync(user, "Test123!");
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, "Organizacion").Wait();
            }
            //--------------------------------------------------------------------------
            if (userManager.FindByNameAsync("org2@example.com").Result == null)
            {
                var user = new User
                {
                    Email = "org2@example.com",
                    UserName = "org2@example.com",
                    EmailConfirmed = true,

                };

                IdentityResult result = await userManager.CreateAsync(user, "Test123!");
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, "Organizacion").Wait();
                //-------------------------------------------------------------
                //Create and save type of adoptante into the database
                var tipoadoptante = new TipoAdoptante
                {
                    Nombre = "Organizacion"
                    
                };

                db.Add(tipoadoptante);
                await db.SaveChangesAsync();

                //-------------------------------------------------------------
                //Create and save Adoptante into the database
                var adoptante = new Adoptante
                {
                    Nombre = user.UserName,
                    TipoAdoptante = db.TipoAdoptante.Where(a => a.Nombre == tipoadoptante.Nombre).FirstOrDefault()

                };

                db.Add(adoptante);
                await db.SaveChangesAsync();
                //-------------------------------------------------------------
                //Create and save AdoptanteUser into the database
                var adoptanteUser = new AdoptanteUser
                {
                    AdoptanteId = adoptante.Id,
                    UserId = user.Id

                };

                db.Add(adoptanteUser);
                await db.SaveChangesAsync();
            }
            //--------------------------------------------------------------------------
            if (userManager.FindByNameAsync("coord@example.com").Result == null)
            {
                var user = new User
                {
                    Email = "coord@example.com",
                    UserName = "coord@example.com",
                    EmailConfirmed = true,

                };

                IdentityResult result = await userManager.CreateAsync(user, "Test123!");
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, "Coordinador").Wait();
            }
            if (userManager.FindByNameAsync("director@example.com").Result == null)
            {
                var user = new User
                {
                    Email = "director@example.com",
                    UserName = "director@example.com",
                    EmailConfirmed = true,

                };

                IdentityResult result = await userManager.CreateAsync(user, "Test123!");
                if (result.Succeeded)
                    userManager.AddToRoleAsync(user, "Director").Wait();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using DespensasCore.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DespensasCore.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Candidato> Candidatos { get; set; }
        public DbSet<Encuesta> Encuestas { get; set; }
        public DbSet<Pregunta> Preguntas { get; set; }
        public DbSet<Asignado> Asignados { get; set; }
        public DbSet<PreguntaItem> PreguntaItems { get; set; }
        public DbSet<RespuestaItem> RespuestaItems { get; set; }
        public DbSet<AsignadoAdoptante> AsignadoAdoptante { get; set; }
        public DbSet<Adoptante> Adoptantes { get; set; }
        public DbSet<AdoptanteUser> AdoptanteUsers { get; set; }
        public DbSet<Colonia> Colonia { get; set; }
        public DbSet<Situacion> Situacion { get; set; }
        public DbSet<TipoAdoptante> TipoAdoptante { get; set; }
        public DbSet<TipoReportante> TipoReportante { get; set; }
        public DbSet<Proyecto> Proyectos { get; set; }
        public DbSet<PersonaProyecto> PersonaProyectos { get; set; }
        public DbSet<UsuarioProyecto> UsuarioProyectos { get; set; }
        public DbSet<Donante> Donantes { get; set; }
        public DbSet<TipoDonante> TipoDonantes { get; set; }
        public DbSet<PeriodicidadDonacion> PeriodicidadDonaciones { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }
        public DbSet<IngresoEgreso> IngresoEgresos { get; set; }
        public DbSet<BankAccount> BankAccounts { get; set; }

        public DbSet<Programa> Programas { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Etapa> Etapas { get; set; }
        public DbSet<Resultado> Resultados { get; set; }
        public DbSet<Actividad> Actividades { get; set; }
        public DbSet<Tarea> Tareas { get; set; }

    }
}